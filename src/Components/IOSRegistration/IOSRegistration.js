import React, { Component } from 'react';
import LogoMenuBroker from '../../Images/LogoMenu_BrokerApp.svg';
import './IOSRegistration.css'

export default class IOSRegistration extends Component {
    render() {
        return (
            <div className="lf-rt-ios">
                <div className="lf-ios-reg-main">
                    <img src={LogoMenuBroker} alt="no_img" />
                    <p className="cnt-lf-para">
                        Welcome To The Assisted Setup To Get The Early Version
                        Of The BrokerApp On Your IPhone. Will You Be Doing Following This Setup Page
                        On Your Desktop While Setting Up The App On Your Iphone?
                        </p>
                    <button className="lf-s-btn btn">Yes</button>
                    <p className="ios-vi-dw">No I Am Viewing This On My IPhone</p>
                </div>
                <div className="rt-ios-reg-main">
                    <div className="step-setup-pr">
                        <div className="d-flex justify-content-center">
                            <i className="fab fa-apple mr-2" style={{ fontSize: "1.6rem" }}></i> <h4>IOS Setup Process</h4>
                        </div>
                        <div className="all-ios-reg-setp">
                            <div className="ios-reg-steps">Step 1: Download Testflight</div>
                            <div className="ios-reg-steps">Step 2: Click Early Access Apple Invitation</div>
                            <div className="ios-reg-steps">Step 3: Open App & Click Login</div>
                            <div className="ios-reg-steps">Step 4: Click I Don’t Have An Account</div>
                            <div className="ios-reg-steps">Step 5: Enter BrokerSync Code</div>
                            <div className="ios-reg-steps">Step 6: Complete Normal GX Registration</div>
                        </div>
                    </div>
                    <div className="wct-vid-ios-reg">
                        Watch Video
                    </div>
                </div>
            </div>
        )
    }
}
