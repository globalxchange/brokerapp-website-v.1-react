import React, { Component } from 'react'
import Dialog from '@material-ui/core/Dialog';
import { ProductConsumer } from '../../Context_Api/Context';
import './PasswordEntry.css';
import { Input } from 'antd';

export default class PasswordEntry extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { passwordEnter, passwordCancel, inputChange, EnterPassword } = value;
                    const PassOkEntry = EnterPassword==="pankaj123"
                    console.log('EnterPassword',EnterPassword)
                    return (
                        <div>
                            <Dialog
                                open={passwordEnter}
                                onClose={passwordCancel}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <div className="p-4">
                                    <h6>Enter Password</h6>
                                    <Input type="password" placeholder="Enter Password" name="EnterPassword" onChange={inputChange} />
                                    <a href="/leaderboard"><button disabled={!PassOkEntry} className="btn-password btn">OK</button></a>
                                    <button className="btn-password btn ml-3" onClick={passwordCancel}>Cancel</button>
                                </div>
                            </Dialog>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
