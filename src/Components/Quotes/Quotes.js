import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../../Context_Api/Context';

export default class Quotes extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft" : "move-marg-bef"}>
                            <div style={{ backgroundColor: "#f9f9f9", minHeight: "100vh", paddingTop: "76px" }}>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-3 col-sm-3 col-xs-12">
                                            <div className="left-short-info">
                                                <h2 className="text-success">$150</h2>
                                                <h6 className="text-success">SAVINGS ESTIMATE</h6>
                                                <h2 className="text-primary">60%</h2>
                                                <h6 className="text-primary">ACCURACY CONFIDENCE</h6>
                                            </div>
                                            <div className="left-short-info">
                                                CARS
                                        </div>
                                            <div className="left-short-info">
                                                DRIVERS
                                        </div>
                                            <div className="left-short-info">
                                                DISCOUNTS
                                        </div>
                                            <div className="left-short-info">
                                                QUOTES
                                        </div>
                                            <div className="left-short-info">
                                                START OVER
                                        </div>
                                        </div>
                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                            <div className="card-big-info">
                                                <div>
                                                    <h2 className="car-info-head">Save your profile to get your quotes!</h2>
                                                    <h5 style={{ fontWeight: "100", textAlign: "center" }}>Don't worry, we hate spam too and will never share your email</h5>
                                                </div>
                                                <div className="my-3">
                                                    <form>
                                                        <div className="row">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                My email
                                                        </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                <input className="px-3 py-2 w-75" type="text" placeholder="Enter your email" />
                                                                <div className="d-flex w-75" style={{ marginTop: "4px" }}>
                                                                    <input type="checkbox" />
                                                                    <label style={{ color: "#7e7e7e", fontSize: "12px" }}>
                                                                        I'd like to receive my quotes and money saving tips via email.
                                                                </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                My phone
                                                        </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                <input className="px-3 py-2 w-75" type="text" placeholder="Enter phone" />
                                                                <div className="d-flex w-75" style={{ marginTop: "4px" }}>
                                                                    <input type="checkbox" />
                                                                    <label style={{ color: "#7e7e7e", fontSize: "12px" }}>
                                                                        I agree to receive text message notification about my insurance quotes from Insurify.
                                                                        I understand that these messages are only informational, and is not required to purchase
                                                                        an insurance policy.
                                                                </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                My Garaging Address
                                                        </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                <input className="px-3 py-2 w-75" type="text" placeholder="Enter Address" />
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div className="container">
                                                    <div className="d-flex justify-content-center align-items-center flex-column mt-3">
                                                        <Link to="/allquotes" className="btn btn-warning mt-5" style={{ width: "16rem" }}>GET QUOTES</Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
