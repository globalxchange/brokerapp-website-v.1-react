import React, { Component } from 'react';
import { ProductConsumer } from '../../Context_Api/Context';
import './Quotes.css';
import NHQuotesLogo from '../../Images/Nvest_health_logo.png';
import NHQuotesBlue from '../../Images/Nvest_health_Blue.png';
import NHQuotesBlueLogo from '../../Images/Nvest_health_blue_Logo.png';

export default class Quotes extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft" : "move-marg-bef"}>
                            <div style={{ backgroundColor: "#f9f9f9", minHeight: "100vh", paddingTop: "76px" }}>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-3 col-sm-3 col-xs-12">
                                            <div className="left-short-info">
                                                {/* <h2 className="text-success">$250</h2>
                                            <h6 className="text-success">SAVINGS ESTIMATE</h6> */}
                                                <h2 className="text-primary">95%</h2>
                                                <h6 className="text-primary">ACCURACY CONFIDENCE</h6>
                                            </div>
                                            <div>
                                                <small>Choose Coverage</small>
                                            </div>
                                            <div className="d-flex flex-column">
                                                <button className="btn choose-qu-left">
                                                    Minimum protection
                                        </button>
                                                <button className="btn choose-qu-left">
                                                    Standard protection
                                        </button>
                                                <button className="btn choose-qu-left">
                                                    Asset protection
                                        </button>
                                                <button className="btn choose-qu-left">
                                                    Premium protection
                                        </button>
                                                <button className="btn choose-qu-left">
                                                    Custom protection
                                        </button>
                                            </div>
                                        </div>
                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                            <div className="card-all-quotes">
                                                <img src={NHQuotesLogo} style={{ width: "4rem", height: "3rem", padding: "6px" }} alt="no_img" />
                                                <h4 className="mb-0 ml-3">Allstate</h4>
                                                <button className="btn btn-nvestblue ml-auto">Get Quote</button>
                                            </div>
                                            <div className="card-all-quotes">
                                                <img src={NHQuotesBlue} style={{ width: "4rem", height: "3rem", padding: "6px" }} alt="no_img" />
                                                <h4 className="mb-0 ml-3">National Auto Quotes</h4>
                                                <button className="btn btn-nvestblue ml-auto">Get Quote</button>
                                            </div>
                                            <div className="card-all-quotes">
                                                <img src={NHQuotesBlueLogo} style={{ width: "4rem", height: "3rem", padding: "6px" }} alt="no_img" />
                                                <h4 className="mb-0 ml-3">Blue Sky Coverage</h4>
                                                <button className="btn btn-nvestblue ml-auto">Get Quote</button>
                                            </div>
                                            <div className="card-all-quotes">
                                                <img src={NHQuotesLogo} style={{ width: "4rem", height: "3rem", padding: "6px" }} alt="no_img" />
                                                <h4 className="mb-0 ml-3">QuoteWizard</h4>
                                                <button className="btn btn-nvestblue ml-auto">Get Quote</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
