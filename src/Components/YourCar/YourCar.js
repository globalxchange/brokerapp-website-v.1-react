import React, { Component } from 'react';
import './YourCarDrive.css'
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../../Context_Api/Context';
import CommuteWork from '../../Images/commute-to-work.svg';
import CommuteSchool from '../../Images/commute-to-school.svg';
import VacationPleasure from '../../Images/vacation-pleasure.svg';
import RideshareDriver from '../../Images/uber_lyft.svg';

export default class YourCar extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { makeselected, inputChange, eachWayDist, diseachway, payStat, paystat, leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft" : "move-marg-bef"}>
                            <div style={{ backgroundColor: "#f9f9f9", minHeight: "100vh", paddingTop: "76px" }}>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-3 col-sm-3 col-xs-12">
                                            <div className="left-short-info">
                                                <h2 class="text-success">$78</h2>
                                                <h6 class="text-success">SAVINGS ESTIMATE</h6>
                                                <h2 class="text-primary">20%</h2>
                                                <h6 class="text-primary">ACCURACY CONFIDENCE</h6>
                                            </div>
                                            <div className="left-short-info">
                                                CARS
                                            </div>
                                            <div className="left-short-info">
                                                DRIVERS
                                            </div>
                                            <div className="left-short-info">
                                                DISCOUNTS
                                            </div>
                                            <div className="left-short-info">
                                                QUOTES
                                            </div>
                                            <div className="left-short-info">
                                                START OVER
                                            </div>
                                        </div>
                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                            <div className="card-big-info">
                                                <div>
                                                    <ol className="progress-steps">
                                                        <li className="is-car">
                                                            <Link to="/" className="tell-your-link">1. Cars</Link>
                                                        </li>
                                                        <li className="is-driver">
                                                            <Link to="/" className="tell-your-link">2. Drivers</Link>
                                                        </li>
                                                        <li className="">
                                                            <Link to="/" className="tell-your-link">3. Quotes</Link>
                                                        </li>
                                                    </ol>
                                                    <h2 className="car-info-head">Tell us about your {makeselected}</h2>
                                                </div>
                                                <div className="container">
                                                    <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            I primarily use my car to
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">

                                                            <div className="form-check card card-use" onClick={eachWayDist}>
                                                                <input id="commutework" onChange={inputChange} name="primarilyuseof" value="Commute to work" type="radio" required />
                                                                <label htmlFor="commutework">
                                                                    <img src={CommuteWork} alt="no_img" />
                                                                    <p>Commute to work</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use" onClick={eachWayDist}>
                                                                <input id="commuteschool" onChange={inputChange} name="primarilyuseof" value="Commute to school" type="radio" required />
                                                                <label htmlFor="commuteschool">
                                                                    <img src={CommuteSchool} alt="no_img" />
                                                                    <p>Commute to school</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use" onClick={eachWayDist}>
                                                                <input id="vacationpleasure" onChange={inputChange} name="primarilyuseof" value="Vacation Pleasure" type="radio" required />
                                                                <label htmlFor="vacationpleasure">
                                                                    <img src={VacationPleasure} alt="no_img" />
                                                                    <p>Vacation/ pleasure</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use" onClick={eachWayDist}>
                                                                <input id="rideshare" onChange={inputChange} name="primarilyuseof" value="Rideshare Driver" type="radio" required />
                                                                <label htmlFor="rideshare">
                                                                    <img src={RideshareDriver} alt="no_img" />
                                                                    <p>Rideshare Driver</p>
                                                                </label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    {diseachway ? <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            Distance each way
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                                            {['5', '10', '15', '25', '50', '75+'].map((distEW) => {
                                                                return (<div className="form-check car-detl-info" onClick={payStat}>
                                                                    <input id={distEW} onChange={inputChange} name="eachway" type="radio" required />
                                                                    <label htmlFor={distEW}>
                                                                        <h6 className="mb-0">{distEW}</h6>
                                                                    </label>
                                                                </div>)
                                                            })}

                                                        </div>
                                                    </div> : null}
                                                    {paystat ? <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            Car payment status
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                                            {['Paid in full', 'Own, still making payments', 'Lease'].map((cPayS) => {
                                                                return (<div className="form-check car-detl-info">
                                                                    <input id={cPayS} onChange={inputChange} name="CPaySt" type="radio" required />
                                                                    <label htmlFor={cPayS}>
                                                                        <h6 className="mb-0">{cPayS}</h6>
                                                                    </label>
                                                                </div>)
                                                            })}
                                                        </div>
                                                    </div> : null}
                                                    {paystat ? <div className="d-flex justify-content-center align-items-center flex-column mt-3">
                                                        <Link className="btn btn-success" style={{ width: "12rem" }}>+ ADD ANOTHER CAR</Link>
                                                        <Link to="/drivers" className="btn btn-warning mt-5" style={{ width: "16rem" }}>CONTINUE TO DRIVER INFO</Link>
                                                    </div> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}