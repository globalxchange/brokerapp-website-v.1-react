import React, { Component } from 'react';
import './YourCarDrive.css'
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../../Context_Api/Context';
import OwnHome from '../../Images/own-a-home.svg';
import HaveJob from '../../Images/commute-to-work.svg';
import Married from '../../Images/wedding-rings.svg';
import Student from '../../Images/commute-to-school.svg';
import Military from '../../Images/is-military.svg';

export default class Discounts extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { inputChange, discountneed, priorinsurance, priorInsurance, haveInsurance, notHaveInsurance,
                        hasinsuredcurrent, whatcurcarrier, yourCurrentCarrier, yourCurrentCarrierSelected, iscurcarrier,
                        isnotcurcarrier, removeCurrentCarrier, notinsured, leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft" : "move-marg-bef"}>
                            <div style={{ backgroundColor: "#f9f9f9", minHeight: "100vh", paddingTop: "76px" }}>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-3 col-sm-3 col-xs-12">
                                            <div className="left-short-info">
                                                <h2 className="text-success">$131</h2>
                                                <h6 className="text-success">SAVINGS ESTIMATE</h6>
                                                <h2 className="text-primary">50%</h2>
                                                <h6 className="text-primary">ACCURACY CONFIDENCE</h6>
                                            </div>
                                            <div className="left-short-info">
                                                CARS
                                        </div>
                                            <div className="left-short-info">
                                                DRIVERS
                                        </div>
                                            <div className="left-short-info">
                                                DISCOUNTS
                                        </div>
                                            <div className="left-short-info">
                                                QUOTES
                                        </div>
                                            <div className="left-short-info">
                                                START OVER
                                        </div>
                                        </div>
                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                            <div className="card-big-info">
                                                <div>
                                                    <ol className="progress-steps">
                                                        <li className="is-car">
                                                            <Link to="/" className="tell-your-link">1. Cars</Link>
                                                        </li>
                                                        <li className="is-driver">
                                                            <Link to="/" className="tell-your-link">2. Drivers</Link>
                                                        </li>
                                                        <li className="">
                                                            <Link to="/" className="tell-your-link">3. Quotes</Link>
                                                        </li>
                                                    </ol>
                                                    {discountneed ? null : <><h2 className="car-info-head">Please tell us about yourself</h2>
                                                        <h5 style={{ fontWeight: "100", textAlign: "center" }}>Your quotes will be most accurate if we understand your needs better.</h5></>}
                                                    {priorinsurance ? <><h2 className="car-info-head">Tell us about your prior insurance.</h2>
                                                        <h5 style={{ fontWeight: "100", textAlign: "center" }}>Car insurance companies will give you a discount based on this info.</h5></> : null}
                                                </div>
                                                <div className="container">
                                                    {discountneed ? null : <>
                                                        <div className="my-3">
                                                            <div className="form-check card card-use">
                                                                <input id="ownhome" onChange={inputChange} name="discountof" value="Own a home" type="radio" required />
                                                                <label htmlFor="ownhome">
                                                                    <img src={OwnHome} alt="no_img" />
                                                                    <p>Own Home</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use">
                                                                <input id="havejob" onChange={inputChange} name="discountof" value="Have Job" type="radio" required />
                                                                <label htmlFor="havejob">
                                                                    <img src={HaveJob} alt="no_img" />
                                                                    <p>Have Job</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use">
                                                                <input id="married" onChange={inputChange} name="discountof" value="Married" type="radio" required />
                                                                <label htmlFor="married">
                                                                    <img src={Married} alt="no_img" />
                                                                    <p>Married</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use">
                                                                <input id="student" onChange={inputChange} name="discountof" value="Student" type="radio" required />
                                                                <label htmlFor="student">
                                                                    <img src={Student} alt="no_img" />
                                                                    <p>Student</p>
                                                                </label>
                                                            </div>
                                                            <div className="form-check card card-use">
                                                                <input id="military" onChange={inputChange} name="discountof" value="Military Veteran" type="radio" required />
                                                                <label htmlFor="military">
                                                                    <img src={Military} alt="no_img" />
                                                                    <p>Military / Veteran</p>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div className="my-3" style={{ fontSize: "14px" }}>
                                                            <p>
                                                                By clicking the "Continue" button, I give express consent to receive marketing communications regarding
                                                                insurance products and services via live operators, automated telephone dialing systems, pre-recorded voice,
                                                                text messages and/or emails from Insurify.com and/or its marketing partners at the phone number and
                                                                email address provided, including wireless numbers, if applicable, even if I have previously registered the
                                                                provided number on the Do Not Call Registry. I understand that consent is not a condition to receive quotes or
                                                                purchase services or products. To receive quotes without providing consent, please call 866-373-0436.
                                                            </p>
                                                            <p>
                                                                By clicking the “Continue” button, you agree that Insurify and its insurance partners may use other
                                                                information sources to collect information about you in order to calculate accurate prices for your
                                                                insurance including, but not limited to, your driving records, your claims history, consumer reports,
                                                                and/or credit history. You also agree to the website's Privacy Policy and Terms & Conditions
                                                            </p>
                                                        </div>
                                                        <div className="d-flex justify-content-center align-items-center flex-column mt-3">
                                                            <button className="btn btn-warning mt-5" style={{ width: "10rem" }} onClick={priorInsurance}>CONTINUE</button>
                                                        </div>
                                                    </>}
                                                    {priorinsurance ? <>
                                                        <div className="row my-3">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                Are you currently insured?
                                                    </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                <div className="form-check car-detl-info" onClick={haveInsurance}>
                                                                    <input id="yesinsured" onChange={inputChange} name="currentlyinsured" type="radio" required />
                                                                    <label htmlFor="yesinsured">
                                                                        <h6 className="mb-0">Yes I have car insurance</h6>
                                                                    </label>
                                                                </div>
                                                                <div className="form-check car-detl-info" onClick={notHaveInsurance}>
                                                                    <input id="noinsured" onChange={inputChange} name="currentlyinsured" type="radio" required />
                                                                    <label htmlFor="noinsured">
                                                                        <h6 className="mb-0">No I don't</h6>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {hasinsuredcurrent ?
                                                            <div>
                                                                <p className="mb-1">What is your current carrier?</p>
                                                                <input className="px-3 py-2 w-100" type="text" placeholder="Select carrier" onFocus={yourCurrentCarrier} autofocus="true" />
                                                            </div>
                                                            : null}
                                                        {whatcurcarrier ? <div style={{ border: "1px solid #dcdcdc" }}>
                                                            <div className="select-curt-carrier">
                                                                <span>Select carrier</span>
                                                            </div>
                                                            <div className="p-1">
                                                                {[{ clogos: OwnHome, clogoname: 'AAA' }, { clogos: HaveJob, clogoname: '21 Century' },
                                                                { clogos: Married, clogoname: 'Allstate' }, { clogos: Student, clogoname: 'Esurance' }].map((carrierBrand) => {
                                                                    return (<div className="select-current-carrier" key={carrierBrand.clogoname} onClick={() => yourCurrentCarrierSelected(carrierBrand)}>
                                                                        <img src={carrierBrand.clogos} className="img-make-select" alt="no_img" />
                                                                        <p>{carrierBrand.clogoname}</p>
                                                                    </div>)
                                                                })}
                                                            </div>
                                                        </div> : null}
                                                        {isnotcurcarrier ? <div>
                                                            <p className="mb-1">What is your current carrier?</p>
                                                            <div className="remove-current-carrier" onClick={removeCurrentCarrier}>
                                                                <span>{iscurcarrier}</span><span>X</span>
                                                            </div>
                                                        </div> : null}
                                                        {notinsured ? <>
                                                            <div className="row my-3">
                                                                <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                    Why don't you have insurance?
                                                    </div>
                                                                <div className="col-md-9 col-sm-9 col-xs-12">
                                                                    {['Car in storage', 'Coverage lapsed', 'On military duty', 'Buying first car', 'Lived abroad', 'Other'].map((noinsur) => {
                                                                        return (<div className="form-check car-detl-info">
                                                                            <input id={noinsur} onChange={inputChange} name="noinsurance" type="radio" required />
                                                                            <label htmlFor={noinsur}>
                                                                                <h6 className="mb-0">{noinsur}</h6>
                                                                            </label>
                                                                        </div>)
                                                                    })}
                                                                </div>
                                                            </div>
                                                            <div className="row my-3">
                                                                <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                    How long has it been since you had car insurance?
                                                    </div>
                                                                <div className="col-md-9 col-sm-9 col-xs-12">
                                                                    {['Naver had insurance', 'Less than week', 'Less than a month', 'More than a month'].map((hlnoinsur) => {
                                                                        return (<div className="form-check car-detl-info">
                                                                            <input id={hlnoinsur} onChange={inputChange} name="howlongnoinsurance" type="radio" required />
                                                                            <label htmlFor={hlnoinsur}>
                                                                                <h6 className="mb-0">{hlnoinsur}</h6>
                                                                            </label>
                                                                        </div>)
                                                                    })}
                                                                </div>
                                                            </div>
                                                        </> : null}
                                                        <div className="d-flex justify-content-center align-items-center flex-column mt-3">
                                                            <Link to="/quotes" className="btn btn-warning mt-5" style={{ width: "10rem" }}>CONTINUE</Link>
                                                        </div>
                                                    </> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
