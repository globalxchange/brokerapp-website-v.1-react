import React, { Component } from 'react';
import './YourCarDrive.css'
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../../Context_Api/Context';

export default class Drivers extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { inputChange, infoYourself, infoyourself, leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft" : "move-marg-bef"}>
                            <div style={{ backgroundColor: "#f9f9f9", minHeight: "100vh", paddingTop: "76px" }}>
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-3 col-sm-3 col-xs-12">
                                            <div className="left-short-info">
                                                <h2 className="text-success">$96</h2>
                                                <h6 className="text-success">SAVINGS ESTIMATE</h6>
                                                <h2 className="text-primary">35%</h2>
                                                <h6 className="text-primary">ACCURACY CONFIDENCE</h6>
                                            </div>
                                            <div className="left-short-info">
                                                CARS
                                        </div>
                                            <div className="left-short-info">
                                                DRIVERS
                                        </div>
                                            <div className="left-short-info">
                                                DISCOUNTS
                                        </div>
                                            <div className="left-short-info">
                                                QUOTES
                                        </div>
                                            <div className="left-short-info">
                                                START OVER
                                        </div>
                                        </div>
                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                            <div className="card-big-info">
                                                <div>
                                                    <ol className="progress-steps">
                                                        <li className="is-car">
                                                            <Link to="/" className="tell-your-link">1. Cars</Link>
                                                        </li>
                                                        <li className="is-driver">
                                                            <Link to="/" className="tell-your-link">2. Drivers</Link>
                                                        </li>
                                                        <li className="">
                                                            <Link to="/" className="tell-your-link">3. Quotes</Link>
                                                        </li>
                                                    </ol>
                                                    <h2 className="car-info-head">Please tell us about yourself</h2>
                                                    <h5 style={{ fontWeight: "100", textAlign: "center" }}>Your quotes will be most accurate if we understand your needs better.</h5>
                                                </div>
                                                <div className="container">
                                                    <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            My first name is
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                                            <input className="px-3 py-2 w-75" type="text" placeholder="Enter your first name" />
                                                        </div>
                                                    </div>
                                                    <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            and last name is
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                                            <input className="px-3 py-2 w-75" type="text" placeholder="Enter your last name" />
                                                        </div>
                                                    </div>
                                                    <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            born on
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                                            <input className="px-3 py-2 w-75" type="date" style={{ textTransform: "uppercase" }} />
                                                        </div>
                                                    </div>
                                                    <div className="row my-3">
                                                        <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                            and am
                                                    </div>
                                                        <div className="col-md-9 col-sm-9 col-xs-12">
                                                            {['Male', 'Female'].map((gend) => {
                                                                return (<div className="form-check car-detl-info" onClick={infoYourself}>
                                                                    <input id={gend} onChange={inputChange} name="gender" type="radio" required />
                                                                    <label htmlFor={gend}>
                                                                        <h6 className="mb-0">{gend}</h6>
                                                                    </label>
                                                                </div>)
                                                            })}
                                                        </div>
                                                    </div>
                                                    {infoyourself ? <>
                                                        <div className="row my-3">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                my license is
                                                    </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                {['Active', 'Permit', 'Suspended', 'Foreign', 'Expired'].map((mylicense) => {
                                                                    return (<div className="form-check car-detl-info">
                                                                        <input id={mylicense} onChange={inputChange} name="license " type="radio" required />
                                                                        <label htmlFor={mylicense}>
                                                                            <h6 className="mb-0">{mylicense}</h6>
                                                                        </label>
                                                                    </div>)
                                                                })}
                                                            </div>
                                                        </div>
                                                        <div className="row my-3">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                I need SR-22 certificate
                                                    </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                {['Yes', 'No'].map((certifi) => {
                                                                    return (<div className="form-check car-detl-info">
                                                                        <input id={certifi} onChange={inputChange} name="certificate " type="radio" required />
                                                                        <label htmlFor={certifi}>
                                                                            <h6 className="mb-0">{certifi}</h6>
                                                                        </label>
                                                                    </div>)
                                                                })}
                                                            </div>
                                                        </div>
                                                        <div className="row my-3">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                How old were you when you first got a US driver's license
                                                    </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                {['16', '17', '18', '19', '20', '21', '22', '23+'].map((howold) => {
                                                                    return (<div className="form-check car-detl-info">
                                                                        <input id={howold} onChange={inputChange} name="howoldlicense" type="radio" required />
                                                                        <label htmlFor={howold}>
                                                                            <h6 className="mb-0">{howold}</h6>
                                                                        </label>
                                                                    </div>)
                                                                })}
                                                            </div>
                                                        </div>
                                                        <div className="row my-3">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 text-right">
                                                                my credit is
                                                    </div>
                                                            <div className="col-md-9 col-sm-9 col-xs-12">
                                                                {['Excellent', 'Good', 'Average', 'Poor'].map((credit) => {
                                                                    return (<div className="form-check car-detl-info">
                                                                        <input id={credit} onChange={inputChange} name="mycredit" type="radio" required />
                                                                        <label htmlFor={credit}>
                                                                            <h6 className="mb-0">{credit}</h6>
                                                                        </label>
                                                                    </div>)
                                                                })}
                                                            </div>
                                                        </div>
                                                    </> : null}
                                                    <div className="d-flex justify-content-center align-items-center flex-column mt-3">
                                                        <Link to="/discounts" className="btn btn-warning mt-5" style={{ width: "16rem" }}>SAVE AND CONTINUE</Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
