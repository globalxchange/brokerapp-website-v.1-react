import React, { Component } from 'react';
// import './Home.css';
import './NeedBroker.css';
// import Looking_For_Broker from '../../Images/Looking_For_Broker.png';
// import Launch_Brokerage from '../../Images/Launch_Brokerage.png';
import { ProductConsumer } from '../../Context_Api/Context';
import { Link } from 'react-router-dom';
import SearchIcon from '../../Images/SearchIcon.svg';
import NoDataFound from '../../Images/no_data_found.svg';

export default class NeedBroker extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { leftMenuDrw, findBResult, brokerFinding, showfindB, selectedBrokerType, vidRightDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "need-brok-full move-marg-aft move-brok-bef" : `${vidRightDrw?"need-brok-full move-marg-bef move-brok-aft":"need-brok-full move-marg-bef move-brok-bef"}`}>
                            {/* <div> */}
                            <div className="head-n-search">
                                <h1 className="ned-brok-head">What Do You Need A Broker For?</h1>
                                <div className="search-in-full">
                                    <div className="search-input">
                                        <input placeholder="Search Requirements....." onChange={brokerFinding} />
                                        <img src={SearchIcon} alt="no_img" />
                                    </div>
                                    {showfindB ?
                                        <>
                                            {findBResult.map((Fbrok) => {
                                                console.log('findBResult', findBResult)
                                                return (
                                                    <Link to='/broker_industry'>
                                                        <div className="broker-suggestion-mob" onClick={()=>selectedBrokerType(Fbrok)}>
                                                            <img src={Fbrok.formData.icon} alt="no_img" />
                                                            {Fbrok.formData.Name}
                                                        </div>
                                                    </Link>
                                                )
                                            })}
                                        </>
                                        : null}
                                    {findBResult.length === 0 ? <div className="broker-suggestion-mob">Not Found</div> : null}
                                </div>
                            </div>
                            <div className="select-img-find-b">
                                {findBResult.map((Fbrok) => {
                                    return (
                                        <Link to='/broker_industry'>
                                            <div className="select-one-each-find-b" onClick={()=>selectedBrokerType(Fbrok)}>
                                                <img src={Fbrok.formData.icon} alt="no_img" />
                                                <p className="mt-3 mb-0 text-center" style={{ color: "#08152D" }}>{Fbrok.formData.Name}</p>
                                            </div>
                                        </Link>
                                    )
                                })}
                                {findBResult.length === 0 ? 
                                <div className="select-one-each-find-b">
                                    <img src={NoDataFound} alt="no_img" />
                                    <p className="mt-3 mb-0 text-center" style={{ color: "#08152D" }}>No Data Found</p>
                                </div>
                                : null} 
                            </div>
                            {/* </div> */}
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}