import React, { Component } from 'react';
import './BrokerIndustryLand.css';
import { ProductConsumer } from '../../Context_Api/Context';
import { Tabs, Tab } from 'react-bootstrap';
import UserImg from '../../Images/user_img.png';
import SearchIconW from '../../Images/SearchIconW.svg';

export default class BrokerIndustryLand extends Component {
    render() {
        const userImg = <img src={UserImg} className="brk-user-img" alt="no_img" />
        return (
            <ProductConsumer>
                {((value) => {
                    const { selectedBrokerfor, leftMenuDrw, vidRightDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "full-brok-tb move-marg-aft move-brok-bef" : `${vidRightDrw?"full-brok-tb move-marg-bef move-brok-aft":"full-brok-tb move-marg-bef move-brok-bef"}`}>
                            <div className="top-brok-i">
                                <div className="each-broker-industry-flex">
                                    <div className="brk-i-left">
                                        <div>
                                        <h6 className="mt-3">Welcome To The World Of</h6>
                                        <h1>{selectedBrokerfor.formData.Name}</h1>
                                        <p className="dis-mob-inds">
                                            <span style={{ color: "#6E7380" }}>BrokerApp</span>
                                            &nbsp;<i className="fas fa-angle-right"></i>&nbsp;
                                            <span style={{ color: "#6E7380", fontWeight: "700" }}>{selectedBrokerfor.formData.Name}</span>
                                        </p>
                                        <p className="dis-mob-inds">{selectedBrokerfor.formData.description}</p>
                                        </div>
                                    </div>
                                    <div className="brk-i-right">
                                        <div className="brk-tab-rt">
                                        <Tabs defaultActiveKey="1" id="uncontrolled-tab-example">
                                            <Tab eventKey="1" title="Buy" style={{ color: "#6E7380" }}>
                                                <p className="wt-trans">What Type Of Transaction Are You Trying To Perform?</p>
                                                <div className="transaction-input">
                                                    <div className="tra-in-lf"><input placeholder={`${"Ex. Buy \xa0"}${selectedBrokerfor.formData.Name}`} /></div>
                                                    <div className="tra-in-rt"><img src={SearchIconW} alt="no_img" /></div>
                                                </div>
                                                <p className="mb-1">First Time Here? <span style={{fontWeight:"700",cursor:"pointer"}}>Click Here To Learn</span></p>
                                                <p className="mb-1">Been Here Before? <span style={{fontWeight:"700",cursor:"pointer"}}>Go Back To Your Quotes</span></p>
                                            </Tab>
                                            <Tab eventKey="2" title="Sell">
                                                <p className="wt-trans">What Type Of Transaction Are You Trying To Perform?</p>
                                                <div className="transaction-input">
                                                    <div className="tra-in-lf"><input placeholder={`${"Ex. Sell \xa0"}${selectedBrokerfor.formData.Name}`} /></div>
                                                    <div className="tra-in-rt"><img src={SearchIconW} alt="no_img" /></div>
                                                </div>
                                                <p className="mb-1">First Time Here? <span style={{fontWeight:"700",cursor:"pointer"}}>Click Here To Learn</span></p>
                                                <p className="mb-1">Been Here Before? <span style={{fontWeight:"700",cursor:"pointer"}}>Go Back To Your Quotes</span></p>
                                            </Tab>
                                            <Tab eventKey="3" title="Other">

                                            </Tab>
                                        </Tabs>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="bottom-brok-i">
                                <Tabs defaultActiveKey="1" id="uncontrolled-tab-example">
                                    <Tab eventKey="1" title="Brokers">
                                        <div className="broker-users">
                                            <img src={UserImg} className="brk-user-img" alt="no_img" />
                                            {userImg}{userImg}{userImg}{userImg}{userImg}{userImg}
                                        </div>
                                    </Tab>
                                    <Tab eventKey="2" title="Brands">
                                        <div className="broker-users">
                                            <img src={UserImg} className="brk-user-img" alt="no_img" />
                                            {userImg}{userImg}{userImg}{userImg}{userImg}{userImg}
                                        </div>
                                    </Tab>
                                    <Tab eventKey="3" title="Influencers">
                                        <div className="broker-users">
                                            <img src={UserImg} className="brk-user-img" alt="no_img" />
                                            {userImg}{userImg}{userImg}{userImg}{userImg}{userImg}
                                        </div>
                                    </Tab>
                                    <Tab eventKey="4" title="Platforms">
                                        <div className="broker-users">
                                            <img src={UserImg} className="brk-user-img" alt="no_img" />
                                            {userImg}{userImg}{userImg}{userImg}{userImg}{userImg}
                                        </div>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
