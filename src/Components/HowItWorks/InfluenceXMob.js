import React, { Component } from 'react';
import './HowItWorks.css';
import { Tabs, Tab } from 'react-bootstrap';
import InfluenceX from '../../Images/InfluenceX.svg';
import {Link} from 'react-router-dom';

export default class InfluenceXMob extends Component {
    render() {
        return (
            <div style={{paddingTop:"73px",height: window.innerHeight,minHeight:"100vh"}}>
                <div className="img-h-mob-abi" >
                    <img className="i-afbk-hvr" src={InfluenceX} alt="no_img" />
                </div>
                <div className="vid-play-abi">
                    <video width="100%" height="100%" autoplay="1" controls style={{ display: "block" }}>
                        <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
                    </video>
                </div>
                <div className="vid-tabs-abi">
                    <Tabs defaultActiveKey="1" id="uncontrolled-tab-example">
                        <Tab eventKey="1" title="About">
                            <div className="p-3">
                                <h5 className="text-center">InfluenceX is a end to end CRM suite made specifically for online Inlfuencers. 
                                It leverages the power of the BrokerApp ecosystem to curate easy to use tools which cater to the 
                                consistent chaos of being an Inlfuencer.</h5>
                            </div>
                        </Tab>
                        <Tab eventKey="2" title="Comments">
                            <form className="comment-in">
                                <input type="text" placeholder="Write Comment" />
                                <i className="fas fa-paper-plane"></i>
                            </form>
                            <div className="p-3">
                                <div className="d-flex mb-2">
                                    <span className="comment-user"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
                                </div>
                                <div className="d-flex mb-2">
                                    <span className="comment-user"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
                                </div>
                            </div>
                        </Tab>
                        <Tab eventKey="3" title="Related">
                            <div className="p-3">Video Video Video</div>
                        </Tab>
                        <Tab eventKey="4" title="Share">
                            <div className="p-3">Click here to share</div>
                        </Tab>
                    </Tabs>
                </div>
                <Link to="/" className="get-start-btn btn">Get Started</Link>
            </div>
        )
    }
}
