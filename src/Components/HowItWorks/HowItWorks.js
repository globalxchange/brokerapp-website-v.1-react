import React, { Component } from 'react';
import './HowItWorks.css';
import InfluenceX from '../../Images/InfluenceX.svg';
import LogoBroker from '../../Images/Icon_Broker.svg';
import AffiliateBank from '../../Images/AffiliateBank.svg';
import BrokerAppFull_logo from '../../Images/BrokerAppFull_logo.svg';
import Expand from '../../Images/expand.svg';
import ExpandW from '../../Images/expandW.svg';
import { Link } from 'react-router-dom';
import { ProductConsumer } from '../../Context_Api/Context';
// import BrokerTV from '../BrokerTV/BrokerTV';

export default class HowItWorks extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { hoverproduces, showInfluenceX, showBrokerapp, showAffiliateBank, showProducesOut, vidDisplayDrw, leftMenuDrw, vidRightDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft move-brok-bef" : `${vidRightDrw?"move-marg-bef move-brok-aft":"move-marg-bef move-brok-bef"}`}>
                            <div className="h-i-w-desk">
                                <div className="h-i-w-top">
                                    <div className="hiw-img-txt-top"></div>
                                    <div className="hiw-img-txt-top"><h3>We Are The Digital Infastructure For Sales Teams All Over The World</h3></div>
                                    <div className="hiw-img-txt-top"></div>
                                </div>
                                <div className="h-i-w-bottom">
                                    <div className="hiw-img-txt-bottom-n1" ><img src={InfluenceX} alt="no_img" onClick={showInfluenceX} /><p onClick={showInfluenceX}>InfluenceX</p></div>
                                    <div className="hiw-img-txt-bottom-n2" ><img src={LogoBroker} alt="no_img" onClick={showBrokerapp} /><p onClick={showBrokerapp}>BrokerApp</p></div>
                                    <div className="hiw-img-txt-bottom-n3" ><img src={AffiliateBank} alt="no_img" onClick={showAffiliateBank} /><p onClick={showAffiliateBank}>AffiliateBank</p></div>
                                    {/* onMouseEnter={showInfluenceX}
                                    onMouseEnter={showBrokerapp}
                                    onMouseEnter={showAffiliateBank} */}
                                </div>
                                <div className={hoverproduces === 'InfluenceX' ? "d-block influ-info-1" : "influ-info-1"} >
                                    <div className="influ-pull-1">
                                        <div>
                                            <div className="vid-hovr-play">
                                                {/* <i className="fas fa-play" style={{color: "#08152D"}}></i> */}
                                                <i className="fas fa-times fa-2x hvr-n1-X" onClick={showProducesOut}></i>
                                                <video width="100%" height="100%" controls style={{display: "block"}}>
                                                    <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
                                                </video>
                                            </div>
                                            <div className="btm-hvr-cont-1">
                                                <div className="img-h-hvr">
                                                    <img className="i-infux-hvr" src={InfluenceX} alt="no_img" />
                                                    <h5>InfluenceX</h5>
                                                    <img className="expand-icon" src={Expand} alt="no_img" onClick={vidDisplayDrw('vidRightDrw', true)} />
                                                </div>
                                                <p className="m-0 px-4">InfluenceX is a end to end CRM suite made specifically for online Inlfuencers.
                                                It leverages the power of the BrokerApp ecosystem to curate easy to use tools
                                                which cater to the consistent chaos of being an Inlfuencer.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={hoverproduces === 'Brokerapp' ? "d-block influ-info-2" : "influ-info-2"}>
                                    <div className="influ-pull-2">
                                        <div>
                                            <div className="vid-hovr-play">
                                                {/* <i className="fas fa-play" style={{color: "#08152D"}}></i> */}
                                                <i className="fas fa-times fa-2x hvr-n2-X" onClick={showProducesOut}></i>
                                                <video width="100%" height="100%" controls style={{display: "block"}}>
                                                    <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
                                                </video>
                                            </div>
                                            <div className="btm-hvr-cont-2">
                                                <div className="img-h-hvr">
                                                    <img className="i-brk-hvr" src={BrokerAppFull_logo} alt="no_img" />
                                                    <img className="expand-icon" src={ExpandW} alt="no_img" onClick={vidDisplayDrw('vidRightDrw', true)} />
                                                </div>
                                                <p className="m-0 px-4">Brokerapp is a network technology packaged into a cross platform application.
                                                It allows brands and distributors to discover eachother
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={hoverproduces === 'AffiliateBank' ? "d-block influ-info-3" : "influ-info-3"}>
                                    <div className="influ-pull-3">
                                        <div>
                                            <div className="vid-hovr-play">
                                                {/* <i className="fas fa-play" style={{color: "#08152D"}}></i> */}
                                                <i className="fas fa-times fa-2x hvr-n3-X" onClick={showProducesOut}></i>
                                                <video width="100%" height="100%" controls style={{display: "block"}}>
                                                    <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
                                                </video>
                                            </div>
                                            <div className="btm-hvr-cont-3">
                                                <div className="img-h-hvr">
                                                    <img className="i-afbk-hvr" src={AffiliateBank} alt="no_img" />
                                                    <div style={{ lineHeight: "0.8" }}>
                                                        <h5>AFFILIATE</h5>
                                                        <small>BANK</small>
                                                    </div>
                                                    <img className="expand-icon" src={Expand} alt="no_img" onClick={vidDisplayDrw('vidRightDrw', true)} />
                                                </div>
                                                <p className="m-0 px-4">AffiliateBank.com is a digital payroll platform where companies can manage
                                                multicurrency payouts. It also provides a portal whereby influencers can leverage their ability
                                                to monetize to obtain traditional financial services.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* <BrokerTV /> */}
                            </div>
                            <div className="h-i-w-mob">
                                <div className="h-i-w-top-mob" style={{height: window.innerHeight}}>
                                    <div className="hiw-text-link">
                                        <h4>We Are The Digital Infastructure For Sales Teams All Over The World</h4>
                                        <Link to='/explore_produces'>Explore Our Produces <i className="fas fa-angle-double-down ml-3"></i></Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
