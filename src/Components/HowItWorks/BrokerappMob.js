import React, { Component } from 'react';
import './HowItWorks.css';
import { Tabs, Tab } from 'react-bootstrap';
import BrokerAppFull_logo from '../../Images/BrokerAppFull_logo.svg';
import {Link} from 'react-router-dom';

export default class BrokerappMob extends Component {
    render() {
        return (
            <div style={{paddingTop:"73px",height: window.innerHeight,minHeight:"100vh"}}>
                <div className="img-h-mob-abi" style={{backgroundColor:"#08152D"}}>
                    <img className="i-brk-hvr" src={BrokerAppFull_logo} alt="no_img" />
                </div>
                <div className="vid-play-abi">
                    <video width="100%" height="100%" autoplay="1" controls style={{ display: "block" }}>
                        <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
                    </video>
                </div>
                <div className="vid-tabs-abi">
                    <Tabs defaultActiveKey="1" id="uncontrolled-tab-example">
                        <Tab eventKey="1" title="About">
                            <div className="p-3">
                                <h5 className="text-center">Brokerapp is a network technology packaged into a cross platform application. 
                                It allows brands and distributors to discover eachother</h5>
                            </div>
                        </Tab>
                        <Tab eventKey="2" title="Comments">
                            <form className="comment-in">
                                <input type="text" placeholder="Write Comment" />
                                <i className="fas fa-paper-plane"></i>
                            </form>
                            <div className="p-3">
                                <div className="d-flex mb-2">
                                    <span className="comment-user"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
                                </div>
                                <div className="d-flex mb-2">
                                    <span className="comment-user"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
                                </div>
                            </div>
                        </Tab>
                        <Tab eventKey="3" title="Related">
                            <div className="p-3">Video Video Video</div>
                        </Tab>
                        <Tab eventKey="4" title="Share">
                            <div className="p-3">Click here to share</div>
                        </Tab>
                    </Tabs>
                </div>
                <Link to="/" className="get-start-btn btn">Get Started</Link>
            </div>
        )
    }
}
