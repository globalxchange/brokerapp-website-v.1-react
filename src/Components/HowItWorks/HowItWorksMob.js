import React, { Component } from 'react';
import './HowItWorks.css';
import InfluenceX from '../../Images/InfluenceX.svg';
import LogoBroker from '../../Images/Icon_Broker.svg';
import AffiliateBank from '../../Images/AffiliateBank.svg';
import {Link} from 'react-router-dom';

export default class HowItWorksMob extends Component {
    render() {
        return (
            <div className="h-i-w-mob">
                <div className="h-i-w-bottom-mob">
                    <div className="hiw-img-txt-bottom-mob"><Link to="/influencex_m"><img src={InfluenceX} alt="no_img" /><p>InfluenceX</p></Link></div>
                    <div className="hiw-img-txt-bottom-mob"><Link to="/brokerapp_m"><img src={LogoBroker} alt="no_img" /><p>BrokerApp</p></Link></div>
                    <div className="hiw-img-txt-bottom-mob"><Link to="/affiliate_bank_m"><img src={AffiliateBank} alt="no_img" /><p>AffiliateBank</p></Link></div>
                </div>
            </div>
        )
    }
}
