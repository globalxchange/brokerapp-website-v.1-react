import React, { Component } from 'react';
import './HomePage.css';
import { Tabs, Tab } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import NvestHealthLogo from '../../Images/Nvest_health_logo.png';
import NvestHealthBlue from '../../Images/Nvest_health_Blue.png';
import NvestHealthBlueLogo from '../../Images/Nvest_health_blue_Logo.png';
import SliderInsurance from './SliderInsurance';
import { ProductConsumer } from '../../Context_Api/Context';

export default class HomePage extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft" : "move-marg-bef"}>
                            <div className="hometop">
                                <div className="hometop-tabs">
                                    <h1>Compare insurance quotes all in one place. <br /> Quick, easy, free.</h1>
                                    <div className="tab-top-home">
                                        <Tabs defaultActiveKey="car" id="uncontrolled-tab-example" className="justify-content-between">
                                            <Tab eventKey="car" title="Car">
                                                <div className="one-tab">
                                                    <p className="text-left">Start with your zip code to compare car insurance:</p>
                                                    <input type="number" className="in-zip" />
                                                    <Link to="/compareinsurance" className="btn btn-nvestblue w-100">START SAVING</Link>
                                                    <div className="d-flex align-items-center justify-content-around mt-3">
                                                        <div>
                                                            <p className="mb-0">*****</p>
                                                            <p className="mb-0">2,000+ Reviews</p>
                                                        </div>
                                                        <div>
                                                            <p className="mb-0">Been Here Before</p>
                                                            <Link to="/" className="mb-0">Get Back to My Quotes</Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="home" title="Home">
                                                <div className="one-tab">
                                                    <p className="text-left">Start with your zip code to compare home insurance:</p>
                                                    <input type="number" className="in-zip" />
                                                    <Link to="/compareinsurance" className="btn btn-nvestblue w-100">START SAVING</Link>
                                                    <div className="d-flex align-items-center justify-content-around mt-3">
                                                        <div>
                                                            <p className="mb-0">*****</p>
                                                            <p className="mb-0">2,000+ Reviews</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Tab>
                                            <Tab eventKey="life" title="Life">
                                                <div className="one-tab">
                                                    <p className="text-left">Start with your zip code to compare life insurance:</p>
                                                    <input type="number" className="in-zip" />
                                                    <Link to="/compareinsurance" className="btn btn-nvestblue w-100">START SAVING</Link>
                                                    <div className="d-flex align-items-center justify-content-around mt-3">
                                                        <div>
                                                            <p className="mb-0">*****</p>
                                                            <p className="mb-0">2,000+ Reviews</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Tab>
                                        </Tabs>
                                    </div>
                                </div>
                            </div>
                            <div className="py-3">
                                <p className="text-center mb-2">As seen in</p>
                                <div className="d-flex align-item-center justify-content-center">
                                    <img src={NvestHealthLogo} className="img-seen-client" alt="no_img" />
                                    <img src={NvestHealthBlue} className="img-seen-client" alt="no_img" />
                                    <img src={NvestHealthBlueLogo} className="img-seen-client" alt="no_img" />
                                </div>
                            </div>
                            <div className="insurance-working">
                                <h1 className="text-center mb-5">How it works</h1>
                                <div className="container">
                                    <div className="insurance-how-works">
                                        <div className="insurance-how-left">
                                            <ul className="list-bullet">
                                                <li>
                                                    <h6 style={{ fontWeight: "900", fontSize: "1.4rem" }}>Compare personalized quotes</h6>
                                                    <p className="mb-5">Answer a couple of questions, we'll provide accurate live quotes.</p>
                                                </li>
                                                <li>
                                                    <h6 style={{ fontWeight: "900", fontSize: "1.4rem" }}>Get all discounts in one place</h6>
                                                    <p className="mb-5">Get all the discounts you qualify for in one place. Find out which insurer will provide you with the most.</p>
                                                </li>
                                                <li>
                                                    <h6 style={{ fontWeight: "900", fontSize: "1.4rem" }}>Buy online or on the phone</h6>
                                                    <p className="mb-5">Get the policy you want the way you want it, buy online or schedule a call with an agent.</p>
                                                </li>
                                                <button className="btn btn-nvestblue">COMPARE QUOTES</button>
                                            </ul>
                                        </div>
                                        <div className="insurance-how-right"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="container py-5">
                                <h1 className="text-center mb-5">Compare Car Insurance by State</h1>
                                <SliderInsurance />
                                <div className="w-100 d-flex align-item-center justify-content-center">
                                    <button className="btn btn-nvestblue">Show All States</button>
                                </div>
                            </div>
                            <div className="top-rated">
                                <div className="container py-5">
                                    <h1 className="text-center mb-5">Top Rated Companies</h1>
                                    <SliderInsurance />
                                    <div className="w-100 d-flex align-item-center justify-content-center">
                                        <button className="btn btn-nvestblue">Show All (300+)</button>
                                    </div>
                                </div>
                            </div>
                            <div className="container py-5">
                                <h1 className="text-center mb-5">Why compare with Insurify?</h1>
                                <div className="row">
                                    <div className="col-md-4 text-center">
                                        <h5>Serious Savings</h5>
                                        <p>Best rates from top insurers starting from $29/mo</p>
                                    </div>
                                    <div className="col-md-4 text-center">
                                        <h5>No Hidden Fees. No Catch</h5>
                                        <p>What you see is what you get. Compare real prices in one place</p>
                                    </div>
                                    <div className="col-md-4 text-center">
                                        <h5>Discounts</h5>
                                        <p>Get exclusive rates & discounts on Insurify</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4 text-center">
                                        <h5>Quick and Easy</h5>
                                        <p>Best Insurance Website of 2016 and 2017</p>
                                    </div>
                                    <div className="col-md-4 text-center">
                                        <h5>Trusted by Millions</h5>
                                        <p>We’ve already helped millions shop for car insurance</p>
                                    </div>
                                    <div className="col-md-4 text-center">
                                        <h5>Buy Online or Speak with an Agent</h5>
                                        <p>We empower you to buy insurance the way you want to!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
