import React, { Component } from 'react';
import './CompareInsurance.css';
import { ProductConsumer } from '../../Context_Api/Context';
import {Link} from 'react-router-dom';

export default class CompareInsurance extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { yearadd,yearselected,showyear,yearRemove,showmakeall,makeselect,makeAdd,makeselected,
                            showmake,makeRemove,showmodalall,modalselect,modalAdd,showmodal,modalselected,modalRemove,showtrimall,
                            trimselect,trimAdd,showtrim,trimselected,trimRemove,filterList,yearselectfilter,leftMenuDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "add-info move-marg-aft" : "add-info move-marg-bef"}>
                            <div className="head-add-info">
                                <h6>Just 3 quick steps to compare your lowest rates. This will only take a minute</h6>
                            </div>
                            <div className="insurance-add-info">
                                <h2 className="text-center">Anchorage drivers can save up to 33% on car insurance!</h2>
                                <p>Let's start with your car information</p>
                                <div className="in-insurance-info">
                                    {showyear?<span className="remove-st-info" onClick={yearRemove}>
                                        <span>{yearselected}</span><span>X</span>
                                    </span>:null}
                                    {showmake?<span className="remove-st-info" onClick={makeRemove}>
                                        <span>{makeselected}</span><span>X</span>
                                    </span>:null}
                                    {showmodal?<span className="remove-st-info" onClick={modalRemove}>
                                        <span>{modalselected}</span><span>X</span>
                                    </span>:null}
                                    {showtrim?<span className="remove-st-info" onClick={trimRemove}>
                                        <span>{trimselected}</span><span>X</span>
                                    </span>:null}
                                    {showyear?null:<form><input type="text" name="search" placeholder="Car Year" className="in-info-all" onChange={filterList}/></form>}
                                    {showmakeall?<input type="text" placeholder="Car Make" className="in-info-all"/>:null}
                                    {showmodalall?<input type="text" placeholder="Car Modal" className="in-info-all"/>:null}
                                    {showtrimall?<input type="text" placeholder="Car Trim" className="in-info-all"/>:null}
                                </div>
                                <div className="select-insu">
                                    {showyear?null:<span>Select car year</span>}
                                    {showmakeall?<span>Select car make</span>:null}
                                    {showmodalall?<span>Select car modal</span>:null}
                                    {showtrimall?<span>Select car trim</span>:null}
                                </div>
                                <div className="select-year-all">
                                    {showyear?null:<>{yearselectfilter.map((yearsel) => {
                                        return (
                                            <div className="select-year" key={yearsel} onClick={()=>yearadd(yearsel)}>{yearsel}</div>
                                        )
                                    })}</>}
                                    {showmakeall?<div className="p-1">
                                        {makeselect.map((NHbrand)=>{
                                        return(<div className="select-make" onClick={()=>makeAdd(NHbrand)} key={NHbrand.logoname}>
                                            <img src={NHbrand.logos} className="img-make-select" alt="no_img"/>
                                            <p>{NHbrand.logoname}</p>
                                        </div>)})}
                                    </div>:null}
                                    {showmodalall?<>{modalselect.map((modalNH)=>{
                                        return (
                                            <div className="select-modal" key={modalNH} onClick={()=>modalAdd(modalNH)}>{modalNH}</div>
                                        )
                                    })}</>:null}
                                    {showtrimall?<>{trimselect.map((trimNH)=>{
                                        return (
                                            <Link to="/cars" className="trim-next-link">
                                                <div className="select-modal" key={trimNH} onClick={()=>trimAdd(trimNH)}>{trimNH}</div>
                                            </Link>
                                        )
                                    })}</>:null}                       
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
