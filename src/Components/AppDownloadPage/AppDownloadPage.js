import React, { Component } from "react";
import "../NavbarTop/NavbarTop.css";
import "./AppDownloadPage.css";
import { ProductConsumer } from "../../Context_Api/Context";
import LogoBroker from "../../Images/Icon_Broker.svg";
import LogoMenuBroker from "../../Images/LogoMenu_BrokerApp.svg";
import Dialog from "@material-ui/core/Dialog";

export default class AppDownloadPage extends Component {
  render() {
    return (
      <ProductConsumer>
        {(value) => {
          const {
            pinOpen,
            pinClickOpen,
            pinClicClose,
            vipPin,
            inputChange,
            brokerAppUpdates,
          } = value;

          const vipPinEnable = vipPin === "4141";
          return (
            <div className="app-down-page-h">
              <button className="btn-brok-web-ap-d btn" onClick={pinClickOpen}>
                BrokerWeb
              </button>
              <div className="mob-app-dow">
                <div className="m-app-l-h-p">
                  <div className="m-app-brand">
                    <img src={LogoBroker} alt="no_img" />
                  </div>
                  <h4>BrokerApp</h4>
                  <p>Monetize Everything You Do</p>
                </div>
                <div className="dow-m-app-b">
                  <h5>Try Out Our Apps</h5>
                  <a
                    href={brokerAppUpdates?.android_app_link || ""}
                    className="andr-io-m-app btn"
                  >
                    <i className="fab fa-android"></i>
                    <span>Android</span>
                  </a>
                  <a
                    href={brokerAppUpdates?.ios_app_link || ""}
                    className="andr-io-m-app btn"
                  >
                    <i className="fab fa-apple"></i>
                    <span>IOS</span>
                  </a>
                  <a
                    href="https://my.brokerapp.io/"
                    className="andr-io-m-app btn"
                  >
                    <i className="fa fa-desktop"></i>
                    <span>Web</span>
                  </a>
                </div>
              </div>
              <Dialog
                onClose={pinClicClose}
                aria-labelledby="simple-dialog-title"
                open={pinOpen}
              >
                <div className="pin-pop-e">
                  <img src={LogoMenuBroker} alt="no_img" />
                  <p>Enter VIP PIN</p>
                  <input type="text" name="vipPin" onChange={inputChange} />
                </div>
                <a href="/newhome" className="w-100">
                  <button
                    disabled={!vipPinEnable}
                    className="btn-pin-submit btn"
                  >
                    Submit
                  </button>
                </a>
              </Dialog>
            </div>
          );
        }}
      </ProductConsumer>
    );
  }
}
