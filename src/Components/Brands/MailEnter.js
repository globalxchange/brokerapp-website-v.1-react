import React, { Component } from 'react';
import { ProductConsumer } from '../../Context_Api/Context';
import {Link} from 'react-router-dom';

export default class MailEnter extends Component {
    render() {
        return (
            <div>
                <ProductConsumer>
                    {((value) => {
                        const { mailInShow, inputChange, brokerAppProfile, EMRemove } = value;
                        return (
                            <>
                            {mailInShow ?
                                <div className="mail-over">
                                    <h5 className="text-white">Enter Email</h5>
                                    <input className="w-75" onChange={inputChange} name="lincenMail" />
                                    <div>
                                        <Link to="/brand_profile" className="btn-mail-sub btn" onClick={brokerAppProfile}>Submit</Link>
                                        <button className="btn-mail-sub btn" onClick={EMRemove}>Cancel</button>
                                    </div>
                                </div>
                            : null}
                            </>
                        )
                    })}
                </ProductConsumer>
            </div>
        )
    }
}
