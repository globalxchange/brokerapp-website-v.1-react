import React, { Component } from 'react';
import './Brands.css';
// import GlobalXchange_icon from '../../Images/GlobalXchange_icon.svg';
import { ProductConsumer } from '../../Context_Api/Context';
import { Link } from 'react-router-dom';
import MailEnter from './MailEnter';

export default class Brands extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { popBrandInfo, hideBrands, allBrands, showAllbrand,  mailEnter, leftMenuDrw, vidRightDrw } = value;
                    const commingSoon =
                        <div className="p-0 col-sm-4">
                            <div className="main-full-sec" style={{ border: "1px solid #E5E5E5" }}>
                                <div className="dis-overlay-comming-soon">
                                    <h3 className="text-center">Next Brand Coming Soon</h3>
                                    <button className="comming-soon-btn btn">Add Your Brand</button>
                                </div>
                            </div>
                        </div>
                    return (
                        <div className={leftMenuDrw ? "brand-tp-f move-marg-aft move-brok-bef" : `${vidRightDrw?"brand-tp-f move-marg-bef move-brok-aft":"brand-tp-f move-marg-bef move-brok-bef"}`}>
                            <div className="row p-0 m-0">
                                {allBrands.map((Brand) => {
                                    return (
                                        <div className="p-0 col-sm-4">
                                            <div className="main-full-sec" style={{ backgroundColor: `#${Brand.colorCode}` }}>
                                                <div className="dis-overlay" onMouseEnter={() => showAllbrand(Brand)}>
                                                    <img className="profile-image" src={Brand.profilePicWhite} alt="no_image" />
                                                </div>
                                                {popBrandInfo === Brand._id ?
                                                    <div className="hide-over-content" onMouseLeave={hideBrands}>
                                                        <div>
                                                            <div className="tp-over-img-txt">
                                                                <img className="over-img" src={Brand.profilePicWhite} alt="no_image" />
                                                                <h3 className="over-head">{Brand.displayName}</h3>
                                                                <p className="over-para">{Brand.country}</p>
                                                            </div>
                                                            <div className="two-btn-down">
                                                                <div className="w-50" onClick={() => mailEnter(Brand)}>
                                                                    <button className="btn btn-wh-lft">BrokerApp Profile</button>
                                                                </div>
                                                                <div className="w-50">
                                                                    <Link to="/" className="btn btn-inhr-rgt">Website</Link>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                : null}
                                            </div>
                                        </div>
                                    )
                                })}
                                {allBrands.length === 1 ?
                                    <>{commingSoon}{commingSoon}{commingSoon}{commingSoon}{commingSoon}</>
                                    : null}
                                {allBrands.length === 2 ?
                                    <>{commingSoon}{commingSoon}{commingSoon}{commingSoon}</>
                                    : null}
                                {allBrands.length === 3 ?
                                    <>{commingSoon}{commingSoon}{commingSoon}</>
                                    : null}
                                {allBrands.length === 4 ?
                                    <>{commingSoon}{commingSoon}</>
                                    : null}
                                {allBrands.length === 5 ?
                                    <>{commingSoon}</>
                                : null}
                            </div>
                            <MailEnter />
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
