import React, { Component } from 'react';
import './Brands.css';
// import GlobalXchange_icon_B from '../../Images/GlobalXchange_icon_B.svg';
import { Drawer } from '@material-ui/core';
import { ProductConsumer } from '../../Context_Api/Context';
import Compensation_icon from '../../Images/Compensation_icon.svg';
import Products_icon from '../../Images/Products_icon.svg';
import { Link } from 'react-router-dom';
// import LogoBrokerGx from '../../Images/Icon_Broker_Gx.svg';

export default class BrandProfile extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { brandProfDrwL, brandProfDrwR, appProfileInfo, leftMenuDrw, vidRightDrw } = value;
                    return (
                        <div className={leftMenuDrw ? "move-marg-aft move-brok-bef" : `${vidRightDrw?"move-marg-bef move-brok-aft":"move-marg-bef move-brok-bef"}`}>
                            <div className="brand-prof-tp-f" style={{ marginRight: brandProfDrwL || brandProfDrwR ? "500px" : "0px" }}>
                                <div className="bg-build" style={{ backgroundImage: `url(${(appProfileInfo.coverPicURL)})` }}></div>
                                <div className={brandProfDrwL || brandProfDrwR ? "prof-info-aft" : "prof-info-bef"}>
                                    <Link to="/brands" className={brandProfDrwL || brandProfDrwR ? "d-none" : ""}>
                                        <div className="back-to-brand" style={{ color: `#${appProfileInfo.colorCode}` }}>
                                            <i className="fas fa-play fa-flip-horizontal"></i>
                                            <h6 style={{ color: `#${appProfileInfo.colorCode}` }}>All Brands</h6>
                                        </div>
                                    </Link>
                                    <div className={brandProfDrwL || brandProfDrwR ? "prof-logo-name-lda" : "prof-logo-name-ldb"}>
                                        <div className="prof-img"><img src={appProfileInfo.profilePicURL} alt="" /></div>
                                        <h3 className="prof-head" style={{ color: `#${appProfileInfo.colorCode}` }}><i className={brandProfDrwL || brandProfDrwR ? "fas fa-play fa-flip-horizontal" : "d-none"}></i> {appProfileInfo.displayName}</h3>
                                    </div>
                                    <div className={brandProfDrwL || brandProfDrwR ? "prof-para-btns-aft" : "prof-para-btns-bef"}>
                                        <p className={brandProfDrwL || brandProfDrwR ? "prof-para text-left" : "prof-para text-center"}>{appProfileInfo.description}</p>
                                        {/* A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin ownership records are stored in a ledger existing in a form */}
                                        <div className="d-flex justify-content-center">
                                            <BrandDrawL />
                                            <BrandDrawR />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}

class BrandDrawL extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { menuDrawer, brandProfDrwL, brandProfDrwR, appProfileInfo } = value;
                    return (
                        <div>
                            <button className={brandProfDrwL || brandProfDrwR ? "btn-b-prof btn mr-2" : "btn-b-prof btn mx-2"} onClick={menuDrawer('brandProfDrwL', true)} style={{ color: `#${appProfileInfo.colorCode}` }}>
                                <img src={Compensation_icon} alt="no_img" />
                                Compensation
                            </button>
                            <Drawer anchor="right" open={brandProfDrwL} onClose={menuDrawer('brandProfDrwL', false)} className="b-pr-mui">
                                <div className="brand-prof-d">
                                    <h3 className="comp-head">Compensation</h3>
                                    <div className="lice">
                                        <div className="my-2 mr-3">
                                            <h6 style={{ color: "#9A9A9A" }}>What Is The Commission Structure?</h6>
                                            <div className="tranl">
                                                <div className="img-tx-tran">
                                                    <img src={Compensation_icon} alt="no_img" />
                                                    <h6>Transactional</h6>
                                                </div>
                                                <p>A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin.</p>
                                            </div>
                                        </div>
                                        <div className="rev-strem">
                                            <div className="my-2 mr-3">
                                                <h6 style={{ color: "#9A9A9A" }}>What Are The Revenue Streams?</h6>
                                                <div className="tranl">
                                                    <div className="img-tx-tran">
                                                        <img src={Compensation_icon} alt="no_img" />
                                                        <h6>Fiat To Crypto Trades</h6>
                                                    </div>
                                                    <p>A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin.</p>
                                                </div>
                                            </div>
                                            <div className="my-2 mr-3">
                                                <h6 style={{ color: "#9A9A9A" }}>What Are The Revenue Streams?</h6>
                                                <div className="tranl">
                                                    <div className="img-tx-tran">
                                                        <img src={Compensation_icon} alt="no_img" />
                                                        <h6>Fiat To Crypto Trades</h6>
                                                    </div>
                                                    <p>A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Drawer>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}

class BrandDrawR extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { menuDrawer, brandProfDrwR, brandProfDrwL, brandLicen, appProfileInfo } = value;
                    return (
                        <div>
                            <button className={brandProfDrwR || brandProfDrwL ? "btn-b-prof btn mr-2" : "btn-b-prof btn mx-2"} onClick={menuDrawer('brandProfDrwR', true)} style={{ color: `#${appProfileInfo.colorCode}` }}>
                                <img src={Products_icon} alt="no_img" />
                                Licenses
                            </button>
                            <Drawer anchor="right" open={brandProfDrwR} onClose={menuDrawer('brandProfDrwR', false)} className="b-pr-mui">
                                <div className="brand-prof-ins-cy">
                                    <div className="d-flex">
                                        {/* <div style={{ boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)", padding: "10px", height: "57px" }}>
                                            <img src={LogoBrokerGx} alt="no_img" style={{ width: "30px" }} />
                                        </div> */}
                                        <div className="broker-li-head">
                                            <h3>Broker Licenses</h3>
                                            <h6>By Global X Change</h6>
                                        </div>
                                    </div>
                                    {/* <p className="my-4" style={{ color: "#9a9a9a", fontSize: "12px" }}>A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin.</p> */}
                                    <div className="brok-all-lic">
                                        {brandLicen.map((BLic) => {
                                            return (
                                                <div className="brok-lices">
                                                    <div className="img-head-a">
                                                        <img src={BLic.product_icon} alt="no_img" />
                                                        <h5 style={{ color: `#${appProfileInfo.colorCode}` }}>{BLic.product_name}</h5>
                                                    </div>
                                                    <p>{BLic.full_description}</p>
                                                    <div className="d-flex">
                                                        <button className="btn-brk-lice btn">See Expert</button>
                                                        <button className="btn-brk-lice btn">Select</button>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </Drawer>


{/* <div className="brand-prof-ins-cy">
    <div className="d-flex">
        <div style={{ boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)", padding: "10px", height: "57px" }}>
            <img src={LogoBrokerGx} alt="no_img" style={{ width: "30px" }} />
        </div>
        <div className="instB-head">
            <h3>InstaCrypto Brokerage</h3>
            <p>Asset On Which Brokers Can Earn</p>
        </div>
    </div>
    <p className="my-4" style={{ color: "#9a9a9a", fontSize: "12px" }}>A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange wherein individual coin.</p>
    <div>
        <h6 style={{ color: "#2676B1", fontWeight: "700" }}>Licenses</h6>
        <div className="incy-lis">
            <p className="incy-lis-para">Lifetime</p>
            <p className="incy-lis-para" style={{ fontWeight: "700" }}>$999.99</p>
        </div>
        <div className="incy-lis">
            <p className="incy-lis-para">Monthly</p>
            <p className="incy-lis-para" style={{ fontWeight: "700" }}>$99.99/Month</p>
        </div>
    </div>
</div> */}


                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}