import React from 'react';
import { Drawer, List, ListItem, ListItemText } from '@material-ui/core';
import { Navbar, Nav } from 'react-bootstrap';
import './NavbarTop.css';
import LogoBroker from '../../Images/Icon_Broker.svg';
import LogoMenuBroker from '../../Images/LogoMenu_BrokerApp.svg';
import { ProductConsumer } from '../../Context_Api/Context';
import PasswordEntry from '../PasswordEntry/PasswordEntry';
import BrokerTVImg from '../../Images/BrokerTV.svg';
// import BrokerTV from '../BrokerTV/BrokerTV';
import BrokerTVImgW from '../../Images/BrokerTV_W.svg';


export default function TemporaryDrawer() {
  const [sidelist] = React.useState(true)

  const sideList = side => (
    <ProductConsumer>
      {((value) => {
        const { menuDrawer, passwordOk } = value;

        return (
          <div
            className="list"
            role="presentation"
          >
            <List>
              <div onClick={menuDrawer(side, false)} style={{ cursor: "pointer", textAlign: "left" }} className="times-mob-side">
                <i className="fas fa-times fa-2x" ></i>
              </div>
              <ListItem>
                <Nav.Link href="/" className="p-0 w-100">
                  <div className="text-center w-100"><img style={{ height: "2.8rem" }} src={LogoMenuBroker} alt="no_img" /></div>
                </Nav.Link>
              </ListItem>
              <div className="mid-menu">
                <ListItem className="side-list-mf" >
                  <ListItemText>
                    <Nav.Link href="/how_it_works" className="menus-links">How It Works</Nav.Link>
                  </ListItemText>
                </ListItem>
                <ListItem className="side-list-mf" >
                  <ListItemText>
                    <Nav.Link href="/brands" className="menus-links">Brands</Nav.Link>
                  </ListItemText>
                </ListItem>
                <ListItem className="side-list-mf" >
                  <ListItemText>
                    <div href="/" className="menus-links on-desk-m-app" style={{ cursor: "pointer" }} onClick={menuDrawer('mAppLDrw', true)}>Mobile Apps</div>
                    <div href="/" className="menus-links on-mob-m-app" style={{ cursor: "pointer" }} onClick={menuDrawer('mAppLDrwMob', true)}>Mobile Apps</div>
                  </ListItemText>
                </ListItem>
                <ListItem className="side-list-mf" >
                  <ListItemText>
                    <div className="menus-links" style={{ cursor: "pointer" }} onClick={passwordOk}>Leaderboard</div>
                  </ListItemText>
                </ListItem>
              </div>

              <ListItem className="mt-auto flex-column">
                <a href="https://my.brokerapp.io/login" className="log-bt-side btn">Login</a>
                <a href="/register" className="log-bt-side-w btn">Register</a>
              </ListItem>

            </List>
          </div>
        )
      })}
    </ProductConsumer>
  );

//   const BrokerTV = () => 
// (
//     <ProductConsumer>
//       {((value) => {
//         const { vidRightDrw, vidDisplayDrw } = value;
//         return (
//           <Drawer className="draw-vid" anchor="right" open={vidRightDrw} onClose={vidDisplayDrw('vidRightDrw', false)} >
//             <div className="brok-tv-full">
//                 <img src={BrokerTVFull} alt="no_image" />
//                 <i className="fas fa-times fa-2x" onClick={vidDisplayDrw('vidRightDrw', false)}></i>
//             </div>
//             <div className="vid-play-draw">
//               {/* <i className="fas fa-play"></i> */}
//               <video width="100%" height="100%" autoplay="1" controls style={{ display: "block" }}>
//                 <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
//               </video>
//             </div>
//             <div className="vid-tabs">
//               <Tabs defaultActiveKey="1" id="uncontrolled-tab-example">
//                 <Tab eventKey="1" title="Comments">
//                   <form className="comment-in">
//                     <input type="text" placeholder="Write Comment" />
//                     <i className="fas fa-paper-plane"></i>
//                   </form>
//                   <div className="p-3">
//                     <div className="d-flex mb-2">
//                       <span className="comment-user"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
//                     </div>
//                     <div className="d-flex mb-2">
//                       <span className="comment-user"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
//                     </div>
//                   </div>
//                 </Tab>
//                 <Tab eventKey="2" title="Related">
//                   <div className="p-3">Video Video Video</div>
//                 </Tab>
//                 <Tab eventKey="3" title="Share">
//                   <div className="p-3">Click here to share</div>
//                 </Tab>
//               </Tabs>
//             </div>
//           </Drawer>
//         )
//       })}
//     </ProductConsumer>
//     )
  


  return (
    <ProductConsumer>
      {((value) => {
        const { menuDrawer, leftMenuDrw, mAppLDrw, AndoridIos, vidDisplayDrw, vidRightDrw } = value;
        const androidioslink = AndoridIos.filter(data => data.Key === 'brokerapp')
        return (
          <Navbar fixed="top" expand="lg" className={leftMenuDrw ? "nav-full move-marg-aft move-brok-bef" : `${vidRightDrw?"nav-full move-marg-bef move-brok-aft":"nav-full move-marg-bef move-brok-bef"}`}>

            <div style={{ cursor: "pointer" }}><i className="fas fa-bars fa-2x" style={{ color: "#fff" }} onClick={menuDrawer('leftMenuDrw', true)}></i></div>
            <Drawer className="par-mui" open={leftMenuDrw} onClose={menuDrawer('leftMenuDrw', false)}>
              {sidelist ? sideList('leftMenuDrw') : null}
            </Drawer>

            <Navbar.Brand className="mx-auto" href="/"><img className="brand-broker" src={LogoBroker} alt="no_img" /></Navbar.Brand>

            <button className="m-app-btn btn" onClick={vidDisplayDrw('vidRightDrw', true)}>Broker <img className="broker-tv-img" src={BrokerTVImg} alt="no_image"/></button>
            <img className="broker-tv-mob-img" src={BrokerTVImgW}  onClick={vidDisplayDrw('vidRightDrw', true)} alt="no_image"/>
            
            <Drawer anchor="top" open={mAppLDrw} onClose={menuDrawer('mAppLDrw', false)} className="m-app-mui">
              <div className="m-app-back" onClick={menuDrawer('mAppLDrw', false)}><i className="fas fa-play fa-flip-horizontal"></i> Go Back</div>
              <div className="mob-app-dow">
                <div className="m-app-l-h-p">
                  <div className="m-app-brand">
                    <img src={LogoBroker} alt="no_img" />
                  </div>
                  <h4>BrokerApp</h4>
                  <p>Monetize Everything You Do</p>
                </div>
                <div className="dow-m-app-b">
                  <h5>Download The App</h5>
                  <a href={androidioslink.map(da => da.formData.androidlink)} download={androidioslink.map(da => da.formData.androidlink)} className="andr-io-m-app btn"><i className="fab fa-android"></i><span>Android</span></a>
                  <a href={androidioslink.map(da => da.formData.ioslink)} download={androidioslink.map(da => da.formData.ioslink)} className="andr-io-m-app btn"><i className="fab fa-apple"></i><span>IOS</span></a>
                </div>
              </div>
            </Drawer>
            {/* <BrokerTV/> */}
            <PasswordEntry/>
          </Navbar>
        )
      })}
    </ProductConsumer>
  );
}