import React, { Component } from 'react';
import './Home.css'
// import Looking_For_Broker from '../../Images/Looking_For_Broker.png';
// import Launch_Brokerage from '../../Images/Launch_Brokerage.png';
import { ProductConsumer } from '../../Context_Api/Context';
import { Link } from 'react-router-dom';
import HomeImg1 from '../../Images/HomeImg1.png';
import HomeImg2 from '../../Images/HomeImg2.svg';
import HomeImg3 from '../../Images/HomeImg3.png';
import { Carousel } from 'antd';

export default class Home extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { leftMenuDrw, vidRightDrw } = value;

                    return (
                        <div className={leftMenuDrw ? "home-full move-marg-aft move-brok-bef" : `${vidRightDrw ? "home-full move-marg-bef move-brok-aft" : "home-full move-marg-bef move-brok-bef"}`}>
                            <div>
                                <h1 className="hm-head">Reimaging Networking <br className="br-dis" /> For The 21st Century</h1>
                                <h5 className="hm-head-second">What Best Describes What You Do?</h5>
                                <div className="select-two-img">
                                    <Link to='/findbroker'>
                                        <div className="select-one-each">
                                            <img src={HomeImg1} width="130px" alt="no_img" />
                                        </div>
                                        <p className="wt-u-do-para" style={{ color: "#08152D" }}>I Am Building A Brand</p>
                                    </Link>
                                    <Link to='/' >
                                        <div className="select-one-each">
                                            <img src={HomeImg2} width="130px" alt="no_img" />
                                        </div>
                                        <p className="wt-u-do-para" style={{ color: "#08152D" }}>I Help Brands Grow</p>
                                    </Link>
                                    <Link to='/' >
                                        <div className="select-one-each">
                                            <img src={HomeImg3} width="130px" alt="no_img" />
                                        </div>
                                        <p className="wt-u-do-para" style={{ color: "#08152D" }}>I Set Trends With New Brands</p>
                                    </Link>
                                </div>
                                <div className=" cur-mob">
                                    <Carousel autoplay dots="0">
                                        <div className="w-100">
                                            <div className="select-one-each">
                                                <img src={HomeImg1} width="130px" alt="no_img" />
                                            </div>
                                            <p className="wt-u-do-para" style={{ color: "#08152D" }}>I Am Building A Brand</p>
                                        </div>
                                        <div className="w-100">
                                            <div className="select-one-each">
                                                <img src={HomeImg2} width="130px" alt="no_img" />
                                            </div>
                                            <p className="wt-u-do-para" style={{ color: "#08152D" }}>I Help Brands Grow</p>
                                        </div>
                                        <div className="w-100">
                                            <div className="select-one-each">
                                                <img src={HomeImg3} width="130px" alt="no_img" />
                                            </div>
                                            <p className="wt-u-do-para" style={{ color: "#08152D" }}>I Set Trends With New Brands</p>
                                        </div>
                                    </Carousel>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
