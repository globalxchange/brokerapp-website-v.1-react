import React, { Component } from 'react';
import './FooterMobile.css';
import '../NavbarTop/NavbarTop.css';
import { ProductConsumer } from '../../Context_Api/Context';
import LogoBroker from '../../Images/Icon_Broker.svg';
import { Drawer } from '@material-ui/core';
// import BrokerTVImg from '../../Images/BrokerTV_W.svg';

export default class FooterMobile extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { menuDrawer, mAppLDrwMob, AndoridIos } = value;
                    const androidioslink = AndoridIos.filter(data => data.Key === 'brokerapp')
                    return (
                        <div>
                            {/* <div className="botm-m-app"><img className="broker-tv-mob-img" src={BrokerTVImg}  onClick={vidDisplayDrw('vidRightDrw', true)} alt="no_image"/></div> */}
                            <Drawer anchor="top" open={mAppLDrwMob} onClose={menuDrawer('mAppLDrwMob', false)} className="m-app-mui">
                                <div className="m-app-back" onClick={menuDrawer('mAppLDrwMob', false)}><i className="fas fa-play fa-flip-horizontal"></i></div>
                                <div className="mob-app-dow">
                                    <div className="m-app-l-h-p">
                                        <div className="m-app-brand">
                                            <img src={LogoBroker} alt="no_img" />
                                        </div>
                                        <h4>BrokerApp</h4>
                                        <p>Please Download The Mobile App For<br/>The Best User Experience.</p>
                                    </div>
                                    <div className="dow-m-app-mob">
                                        <a href={androidioslink.map(da=>da.formData.androidlink)} download={androidioslink.map(da=>da.formData.androidlink)} className="andr-io-m-app btn"><i className="fab fa-android"></i><span>Android</span></a>
                                        <a href={androidioslink.map(da=>da.formData.ioslink)} download={androidioslink.map(da=>da.formData.ioslink)} className="andr-io-m-app btn"><i className="fab fa-apple"></i><span>IOS</span></a>
                                    </div>
                                </div>
                            </Drawer>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
