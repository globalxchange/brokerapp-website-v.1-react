// import React from 'react';
// import PropTypes from 'prop-types';
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';
// import Typography from '@material-ui/core/Typography';
// import Box from '@material-ui/core/Box';
// import './Leaderboard.css';
// import Looking_For_Broker from '../../Images/Looking_For_Broker.png'

// function TabPanel(props) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`vertical-tabpanel-${index}`}
//       aria-labelledby={`vertical-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box p={3} style={{ width: "100%" }}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// }

// TabPanel.propTypes = {
//   children: PropTypes.node,
//   index: PropTypes.any.isRequired,
//   value: PropTypes.any.isRequired,
// };

// function a11yProps(index) {
//   return {
//     id: `vertical-tab-${index}`,
//     'aria-controls': `vertical-tabpanel-${index}`,
//   };
// }


// export default function Leaderboard() {
//   const [value, setValue] = React.useState(0);

//   const handleChange = (event, newValue) => {
//     setValue(newValue);
//   };

//   const lead = <div className="lead-board">
//     <h1>1</h1>
//     <img src={Looking_For_Broker} alt="no_img"/>
//     <div className="lead-user-info">
//       <h6>Jacob Jones</h6>
//       <div>
//         <small>Distence |</small>
//         <small> Distence |</small>
//         <small> Distence</small>
//       </div>
//       <div>
//         <small>3.1m </small>
//         <small>320 </small>
//         <small>13</small>
//       </div>
//     </div>
//   </div>

//   return (
//     <div className="ver-tab-lead">
//       <Tabs
//         orientation="vertical"
//         variant="scrollable"
//         value={value}
//         onChange={handleChange}
//         aria-label="Vertical tabs example"
//       >
//         <Tab label="Item One" {...a11yProps(0)} />
//         <Tab label="Item Two" {...a11yProps(1)} />
//         <Tab label="Item Three" {...a11yProps(2)} />
//         <Tab label="Item Four" {...a11yProps(3)} />
//         <Tab label="Item Five" {...a11yProps(4)} />
//       </Tabs>
//       <TabPanel value={value} index={0} style={{ width: "calc(100% - 160px)" }}>
//         <div className="d-flex">
//           <div>
//           <div className="px-4 py-3 mb-1" style={{ boxShadow: "0 0 10px 4px #CCCCCC", color: "#8996a9" }}><i className="fas fa-trophy"></i> LEADERBOARD</div>
//           <div style={{ boxShadow: "0 0 10px 4px #CCCCCC" }}>
//             <div className="lead-board">
//               <h1>1</h1>
//               <img src={Looking_For_Broker} alt="no_img"/>
//               <div className="lead-user-info">
//                 <h6>Jacob Jones</h6>
//                 <div className="lead-dcr-full">
//                   <div>
//                     <p className="lead-dcr">Distence<span className="mx-2">|</span></p>
//                     <p className="lead-dcr-num">3.1m </p>
//                   </div>
//                   <div>
//                     <p className="lead-dcr">Distence<span className="mx-2">|</span></p>
//                     <p className="lead-dcr-num">320 </p>
//                   </div>
//                   <div>
//                     <p className="lead-dcr">Distence<span className="mx-2">|</span></p>
//                     <p className="lead-dcr-num">13 </p>
//                   </div>
//                 </div>
//               </div>
//             </div>
//             {lead}{lead}{lead}{lead}
//           </div>
//           </div>
//           <div>
//             <div></div>
//           </div>
//         </div>
//       </TabPanel>
//       <TabPanel value={value} index={1}>
//         Item Two
//       </TabPanel>
//       <TabPanel value={value} index={2}>
//         Item Three
//       </TabPanel>
//       <TabPanel value={value} index={3}>
//         Item Four
//       </TabPanel>
//       <TabPanel value={value} index={4}>
//         Item Five
//       </TabPanel>
//     </div>
//   );
// }

import React, { Component } from 'react';
import { Tabs } from 'antd';
import 'antd/dist/antd.css';
import './Leaderboard.css';
import Looking_For_Broker from '../../Images/Looking_For_Broker.png';

export default class AppOverView extends Component {
  render() {
    return (
      <div className="lead-b-tp-f">
        <Demo />
      </div>
    )
  }
}

const lead = <div className="lead-board">
  <h1>1</h1>
  <img src={Looking_For_Broker} alt="no_img" />
  <div className="lead-user-info">
    <h6>Jacob Jones</h6>
    <div className="lead-dcr-full">
      <div>
        <p className="lead-dcr">Distence<span className="mx-2">|</span></p>
        <p className="lead-dcr-num">3.1m </p>
      </div>
      <div>
        <p className="lead-dcr">Calories<span className="mx-2">|</span></p>
        <p className="lead-dcr-num">320 </p>
      </div>
      <div>
        <p className="lead-dcr">Repetitions</p>
        <p className="lead-dcr-num">13 </p>
      </div>
    </div>
  </div>
</div>

const { TabPane } = Tabs;

class Demo extends React.Component {
  // state = {
  //   tabKeyA: 1, tabKeyB: 2, tabKeyC: 3, tabKeyD: 4
  // }
  // keyUpdateA = () => { this.setState({ tabKeyA: 1, tabKeyB: 2, tabKeyC: 3, tabKeyD: 4 }) }
  // keyUpdateB = () => { this.setState({ tabKeyA: 4, tabKeyB: 1, tabKeyC: 2, tabKeyD: 3 }) }
  // keyUpdateC = () => { this.setState({ tabKeyA: 3, tabKeyB: 4, tabKeyC: 1, tabKeyD: 2 }) }
  // keyUpdateD = () => { this.setState({ tabKeyA: 2, tabKeyB: 3, tabKeyC: 4, tabKeyD: 1 }) }

  render() {
    return (
      <div>
        <Tabs tabPosition='left' className="tabs-ver">
          <TabPane 
          tab={<div className="pane-tab-cust">
            <img src={Looking_For_Broker} alt="no_img" />
            <p className="pane-name">Power Circuit</p>
            <p className="pane-time">10:30am - 11:00am</p>
            <p className="pane-up-cls">UPCOMMING CLASS</p>
            </div>} 
            key={1} >
            <div className="lead-flex-divide">
              <div className="lead-flex-left">
                <div className="px-4 py-3 mb-1" style={{ boxShadow: "0 0 10px 4px #CCCCCC", color: "#8996a9" }}><i className="fas fa-trophy"></i> LEADERBOARD</div>
                <div style={{ boxShadow: "0 0 10px 4px #CCCCCC" }}>
                  <div className="lead-board">
                    <h1>1</h1>
                    <img src={Looking_For_Broker} alt="no_img" />
                    <div className="lead-user-info">
                      <h6>Jacob Jones</h6>
                      <div className="lead-dcr-full">
                        <div>
                          <p className="lead-dcr">Distence<span className="mx-2">|</span></p>
                          <p className="lead-dcr-num">3.1m </p>
                        </div>
                        <div>
                          <p className="lead-dcr">Calories<span className="mx-2">|</span></p>
                          <p className="lead-dcr-num">320 </p>
                        </div>
                        <div>
                          <p className="lead-dcr">Repetitions</p>
                          <p className="lead-dcr-num">13 </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  {lead}{lead}{lead}{lead}{lead}
                </div>
              </div>
              <div className="lead-flex-right">
                <div className="p-3" style={{ boxShadow: "0 0 10px 4px #CCCCCC" }}>
                  <p className="m-0">CLASS GOLAS</p>
                  <div className="sliders-hoz"><p></p><small>REPETITIONS</small></div>
                  <div className="sliders-hoz"><p></p><small>CALORIES</small></div>
                  <div className="sliders-hoz"><p></p><small>DISTANCE</small></div>
                </div>
                <div className="p-3 mt-3" style={{ boxShadow: "0 0 10px 4px #CCCCCC" }}>
                  <p className="mb-2">DISTENCE</p>
                  <div className="d-flex align-items-end">
                  <div className="map-ver"></div>
                  <div className="map-ver"></div>
                  <div className="map-ver"></div>
                  <div className="map-ver"></div>
                  <div className="map-ver"></div>
                  </div>
                </div>
                <div className="p-3 mt-3" style={{ boxShadow: "0 0 10px 4px #CCCCCC" }}>
                  <p className="mb-2">CLASSMATES (6)</p>
                  <div className="d-flex align-items-center my-2">
                    <img className="cls-mat" src={Looking_For_Broker} alt="no_img" />
                    <p className="mb-0 ml-3">Jacob Jones</p>
                  </div>
                  <div className="d-flex align-items-center my-2">
                    <img className="cls-mat" src={Looking_For_Broker} alt="no_img" />
                    <p className="mb-0 ml-3">Jacob Jones</p>
                  </div>
                  <div className="d-flex align-items-center my-2">
                    <img className="cls-mat" src={Looking_For_Broker} alt="no_img" />
                    <p className="mb-0 ml-3">Jacob Jones</p>
                  </div>
                  <div className="d-flex align-items-center my-2">
                    <img className="cls-mat" src={Looking_For_Broker} alt="no_img" />
                    <p className="mb-0 ml-3">Jacob Jones</p>
                  </div>
                </div>
              </div>
            </div>
          </TabPane>
          <TabPane tab={
          <div className="pane-tab-cust">
          <img src={Looking_For_Broker} alt="no_img" />
          <p className="pane-name">Power Circuit</p>
          <p className="pane-time">10:30am - 11:00am</p>
          <p className="pane-up-cls">UPCOMMING CLASS</p>
          </div>
          } key={2} >
            2
          </TabPane>
          <TabPane tab={
            <div className="pane-tab-cust">
            <img src={Looking_For_Broker} alt="no_img" />
            <p className="pane-name">Power Circuit</p>
            <p className="pane-time">10:30am - 11:00am</p>
            <p className="pane-up-cls">UPCOMMING CLASS</p>
            </div>
          } key={3} >
            3
          </TabPane>
          <TabPane tab={
          <div className="pane-tab-cust">
          <img src={Looking_For_Broker} alt="no_img" />
          <p className="pane-name">Power Circuit</p>
          <p className="pane-time">10:30am - 11:00am</p>
          <p className="pane-up-cls">UPCOMMING CLASS</p>
          </div>
          } key={4} >
            4
          </TabPane>
          <TabPane tab={
          <div className="pane-tab-cust">
          <img src={Looking_For_Broker} alt="no_img" />
          <p className="pane-name">Power Circuit</p>
          <p className="pane-time">10:30am - 11:00am</p>
          <p className="pane-up-cls">UPCOMMING CLASS</p>
          </div>
          } key={5} >
            5
          </TabPane>
        </Tabs>
      </div>
    );
  }
}