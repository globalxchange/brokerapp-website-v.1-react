import React, { Component } from 'react';
import './LoginRegister.css';
import LogoFullBroker from '../../Images/LogoMenu_BrokerApp.svg';
import { ProductConsumer } from '../../Context_Api/Context';
import {Link} from 'react-router-dom';

export default class EnterCode extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { inputChange } = value;
                    // const loginEnabled = logpassword.length > 2 && logemail.length > 4
                    return (
                        <div>
                            <div className="login-ag">
                                <div>
                                    <img src={LogoFullBroker} style={{ width: "20rem" }} alt="no_img" />
                                    <form className="w-100 mt-5">
                                        <div className="form-group">
                                            <input onChange={inputChange} type="text" name="gxRegUsername" className="login-in-user" placeholder="Create Broker Username" />
                                        </div>
                                        <div className="form-group">
                                            <input onChange={inputChange} type="email" name="gxRegEmail" className="login-in-mail" placeholder="Enter Email" />
                                        </div>
                                    </form>
                                    <Link to="/register_password" className="login-in-login btn">Next</Link>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
