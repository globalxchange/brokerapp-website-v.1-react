import React, { Component } from 'react';
import './LoginRegister.css';
import LogoFullBroker from '../../Images/LogoMenu_BrokerApp.svg';
import { ProductConsumer } from '../../Context_Api/Context';
import {Link} from 'react-router-dom';

export default class Password extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { inputChange } = value;
                    // const loginEnabled = logpassword.length > 2 && logemail.length > 4
                    return (
                        <div>
                            <div className="login-ag">
                                <div>
                                    <img src={LogoFullBroker} style={{ width: "20rem" }} alt="no_img" />
                                    <form className="w-100 mt-5">
                                        <div className="form-group">
                                            <input onChange={inputChange} type="password" name="gxRegCreatepass" className="login-in-pass" placeholder="Create Password" />
                                        </div>
                                    </form>
                                    <Link to="/register_newpassword" className="login-in-login btn">Next</Link>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}