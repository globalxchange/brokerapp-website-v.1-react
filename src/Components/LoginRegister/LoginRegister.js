import React, { Component } from 'react';
import './LoginRegister.css';
import LogoFullBroker from '../../Images/LogoMenu_BrokerApp.svg';
import { ProductConsumer } from '../../Context_Api/Context';
// import { Link } from 'react-router-dom';
import { LoadingOutlined } from '@ant-design/icons';

export default class EnterCode extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { inputChange, registring, gxUserMailReg, gxPasswordReg, gxNewPasswordReg, GxRegistration, CheckLoad, mailCodeGxRegistration,
                            gxCode, gxRegUsername, gxRegEmail, gxRegCreatepass, gxRegConfirmpass, gxRegEmailCode } = value;
                    const gxCodeEnabled = gxCode.length > 0
                    const gxUserMailEnable = gxRegUsername.length > 2 && gxRegEmail.length > 4
                    const gxPasswordEnable = gxRegCreatepass.length > 2 
                    const gxConPasswordEnable = gxRegCreatepass === gxRegConfirmpass
                    const gxEmailCodeEnable = gxRegEmailCode.length > 2
                    return (
                        <div>
                            <div className="login-ag">
                                <div>
                                    <img src={LogoFullBroker} style={{ width: "20rem" }} alt="no_img" />

                                    {registring === "gCode" ?
                                        <>
                                        <p className="text-center m-0">Please Enter The BrokerSync Code From <br />Your Broker</p>
                                            <form className="w-100 mt-5">
                                                <div className="form-group">
                                                    <input onChange={inputChange} type="text" name="gxCode" className="login-in-et-code" placeholder="Enter Code.." />
                                                </div>
                                            </form>
                                            <button disabled={!gxCodeEnabled} onClick={gxUserMailReg} className="login-in-login btn">Next</button>
                                        </>
                                        : null}
                                    {registring === "gUserMail" ?
                                        <>
                                            <form className="w-100 mt-5">
                                                <div className="form-group">
                                                    <input onChange={inputChange} type="text" name="gxRegUsername" className="login-in-user" placeholder="Create Broker Username" />
                                                </div>
                                                <div className="form-group">
                                                    <input onChange={inputChange} type="email" name="gxRegEmail" className="login-in-mail" placeholder="Enter Email" />
                                                </div>
                                            </form>
                                            <button disabled={!gxUserMailEnable} onClick={gxPasswordReg} className="login-in-login btn">Next</button>
                                        </>
                                        : null}
                                    {registring === "gPassword" ?
                                        <>
                                            <form className="w-100 mt-5">
                                                <div className="form-group">
                                                    <input onChange={inputChange} type="password" name="gxRegCreatepass" className="login-in-pass" placeholder="Create Password" />
                                                </div>
                                            </form>
                                            <button disabled={!gxPasswordEnable} onClick={gxNewPasswordReg} className="login-in-login btn">Next</button>
                                        </>
                                    : null}
                                    {registring === "gNewPassword" ?
                                        <>
                                            <form className="w-100 mt-5">
                                                <div className="form-group">
                                                    <input onChange={inputChange} type="password" name="gxRegConfirmpass" className="login-in-pass" placeholder="Confirm Password" />
                                                </div>
                                            </form>
                                            <button disabled={!gxConPasswordEnable} onClick={GxRegistration} className="login-in-login btn">Next</button>
                                        </>
                                    : null}
                                    {CheckLoad? <div className="checkload-anim"><LoadingOutlined style={{ fontSize: 50 }} spin /></div> :null}
                                    {registring === "gEmailCode" ?
                                        <>
                                            <form className="w-100 mt-5">
                                                <div className="form-group">
                                                    <input onChange={inputChange} type="text" name="gxRegEmailCode" className="login-in-pass" placeholder="Enter The Six Digit Code From Your Email" />
                                                </div>
                                            </form>
                                            <button disabled={!gxEmailCodeEnable} onClick={mailCodeGxRegistration} className="login-in-login btn">Next</button>
                                        </>
                                    : null}
                                    {registring === "gCongrats" ?
                                        <>
                                            <p>Congrats. You Have Completed Registration</p>
                                            <a href="https://app.brokerapp.io/login" className="login-in-login btn">Login</a>
                                        </>
                                    : null}
                                </div>
                            </div>
                        </div>
                    )
                })}
            </ProductConsumer>
        )
    }
}
