import React, { Component } from 'react';
import './BrokerTV.css';
import BrokerTVFull from '../../Images/BrokerTVFull.svg';
import { ProductConsumer } from '../../Context_Api/Context';
import Drawer from '@material-ui/core/Drawer';
import { Tabs, Tab } from 'react-bootstrap';
import BTV_Content from '../../Images/BTV_Content.svg';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from "@material-ui/core/styles";
import MailEnter from '../Brands/MailEnter';

export default class BrokerTV extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { vidRightDrw, vidDisplayDrw, onBrokerTV, allBrands, moveBrokerTV, tvBrand, moveBrokerGuideC, mailEnterProfile } = value;
                    const EditTooltip = withStyles({
                        tooltip: {
                            color: "#08152D",
                            backgroundColor: "transparent",
                            fontSize: "12px",
                        }
                    })(Tooltip);
                    const btvcontent =
                        <EditTooltip placement="bottom" title="Apply Pressure" onClick={moveBrokerTV}>
                            <div className="contn-brd-img" style={{ backgroundColor: "#000000" }}>
                                <img className="contn-img" src={BTV_Content} alt="no_img" />
                            </div>
                        </EditTooltip>
                    return (
                        <Drawer className="draw-vid" anchor="right" open={vidRightDrw} >
                            {onBrokerTV === 'onContent' ?
                                <div className="brok-tv-all-con">
                                    {/* <div className="d-flex"><i className="fas fa-times fa-2x ml-auto" onClick={vidDisplayDrw('vidRightDrw', false)}></i></div> */}
                                    <div className="conten-all-btv">
                                        <div className="w-100">
                                            <h5>Trending Content</h5>
                                            <div className="set-con-brn">
                                                <EditTooltip placement="bottom" title="Apply Pressure" onClick={moveBrokerTV}>
                                                    <div className="contn-brd-img" style={{ backgroundColor: "#000000" }}>
                                                        <img className="contn-img" src={BTV_Content} alt="no_img" />
                                                    </div>
                                                </EditTooltip>
                                                {btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}
                                            </div>
                                        </div>
                                        <div className="w-100">
                                            <h5>Live Content</h5>
                                            <div className="set-con-brn">
                                                <EditTooltip placement="bottom" title="Apply Pressure" onClick={moveBrokerTV}>
                                                    <div className="contn-brd-img" style={{ backgroundColor: "#000000" }}>
                                                        <img className="contn-img" src={BTV_Content} alt="no_img" />
                                                    </div>
                                                </EditTooltip>
                                                {btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}
                                            </div>
                                        </div>
                                        <div className="w-100">
                                            <h5>All Brand Channels</h5>
                                            <div className="set-con-brn">
                                                {allBrands.map((Brand) => {
                                                    return (
                                                        <EditTooltip placement="bottom" title={Brand.displayName} onClick={() => moveBrokerTV(Brand)}>
                                                            <div className="contn-brd-img" style={{ backgroundColor: `#${Brand.colorCode}` }}>
                                                                <img className="contn-img" src={Brand.profilePicWhite} alt="no_img" />
                                                            </div>
                                                        </EditTooltip>
                                                    )
                                                })}
                                                {btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}{btvcontent}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                : null}
                            {onBrokerTV === 'onVideo' ?
                                <>
                                    <div className="brok-tv-full">
                                        {tvBrand.length === 0 ? <img src={BrokerTVFull} alt="no_image" /> : <img src={tvBrand.profilePicURL} alt="no_image" />}
                                        {/* <i className="fas fa-times fa-2x" onClick={vidDisplayDrw('vidRightDrw', false)}></i> */}
                                    </div>
                                    <div className="vid-play-draw">
                                        <video width="100%" height="100%" autoplay="1" controls style={{ display: "block" }}>
                                            <source src="https://toshimarkets.nyc3.cdn.digitaloceanspaces.com/Video%20On%20Setupmywallet.com.mp4" type="video/mp4" />
                                        </video>
                                    </div>
                                    <div className="vid-tabs" style={{ paddingBottom: "55px" }}>
                                        <Tabs defaultActiveKey="1" id="uncontrolled-tab-example">
                                            <Tab eventKey="1" title="Featured">
                                                <div style={{ padding: "1rem 65px" }}>
                                                    <h6 style={{ color: `#${tvBrand.colorCode}` }}>Welcome To {tvBrand.displayName} Channel</h6>
                                                    <p>{tvBrand.description}</p>
                                                </div>
                                                <div className="play-lis-full">
                                                    <h6 style={{ color: `#${tvBrand.colorCode}` }}>Featured Playlists</h6>
                                                    <div className="ft-p-list">
                                                        <div>
                                                            <div className="ft-list-vid"></div>
                                                            <div className="p-1">
                                                                <h6 style={{ color: `#${tvBrand.colorCode}` }}>Introduction To Coinbase</h6>
                                                                <p>In This Episode We Are Thrilled To Award Larry Bird With 1 Billion Dollars In Bitcoin</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="ft-p-list">
                                                        <div>
                                                            <div className="ft-list-vid"></div>
                                                            <div className="p-1">
                                                                <h6 style={{ color: `#${tvBrand.colorCode}` }}>Introduction To Coinbase</h6>
                                                                <p>In This Episode We Are Thrilled To Award Larry Bird With 1 Billion Dollars In Bitcoin</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* <form className="comment-in-brk">
                                                        <input type="text" placeholder="Write Comment" />
                                                        <i className="fas fa-paper-plane"></i>
                                                    </form>
                                                    <div className="p-3">
                                                    <div className="d-flex mb-2">
                                                        <span className="comment-user-brk"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
                                                    </div>
                                                    <div className="d-flex mb-2">
                                                        <span className="comment-user-brk"></span><p className="m-0">Lorem Ipsum is simply dummy text of the printing</p>
                                                    </div>
                                                </div> */}
                                            </Tab>
                                            <Tab eventKey="2" title="Catalogue">
                                                <div className="p-3">Video Video Video</div>
                                            </Tab>
                                            <Tab eventKey="3" title="Meetings">
                                                <div className="p-3">Click here to share</div>
                                            </Tab>
                                            <Tab eventKey="4" title="Brand">
                                                <div className="p-3">Click here to Brand</div>
                                            </Tab>
                                        </Tabs>
                                    </div>
                                    <div className="brok-tv-bott" style={{ backgroundColor: `#${tvBrand.colorCode}` }}>
                                        <i className="fas fa-power-off off-close-btv" onClick={vidDisplayDrw('vidRightDrw', false)}></i>
                                        <div className="chan-gui" onClick={moveBrokerGuideC}><i className="fas fa-play fa-flip-horizontal mr-2"></i> <span>Channel Guide</span></div>
                                        <div className="prof-brd-v" onClick={() => mailEnterProfile(tvBrand)}>Profile</div>
                                    </div>
                                </>
                                : null}
                            <MailEnter />
                        </Drawer>
                    )
                })}
            </ProductConsumer>
        )
    }
}