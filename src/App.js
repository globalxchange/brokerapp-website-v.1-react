import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import NavbarTop from './Components/NavbarTop/NavbarTop';
import Home from './Components/Home/Home';
import Homepage from './Components/HomePage/HomePage';
import CompareInsurance from './Components/CompareInsurance/CompareInsurance';
import YourCar from './Components/YourCar/YourCar';
import Drivers from './Components/YourCar/Drivers';
import Discounts from './Components/YourCar/Discounts';
import Quotes from './Components/Quotes/Quotes';
import AllQuotes from './Components/Quotes/AllQuotes';
import FindABroker from './Components/BrokerIndustry/NeedBroker';
import HowItWorks from './Components/HowItWorks/HowItWorks';
import HowItWorksMob from './Components/HowItWorks/HowItWorksMob';
import Leaderboard from './Components/Leaderboard/Leaderboard';
import AffiliateBankMob from './Components/HowItWorks/AffiliateBankMob';
import InfluenceXMob from './Components/HowItWorks/InfluenceXMob';
import BrokerappMob from './Components/HowItWorks/BrokerappMob';
import BrokerIndustryLand from './Components/BrokerIndustry/BrokerIndustryLand';
import Brands from './Components/Brands/Brands';
import BrandProfile from './Components/Brands/BrandProfile';
import FooterMobile from './Components/FooterMobile/FooterMobile';
import BrokerTV from './Components/BrokerTV/BrokerTV';
import EnterCode from './Components/LoginRegister/LoginRegister';
import UserMail from './Components/LoginRegister/UserMail';
import Password from './Components/LoginRegister/Password';
import NewPassword from './Components/LoginRegister/NewPassword';
import IOSRegistration from './Components/IOSRegistration/IOSRegistration';
import AppDownloadPage from './Components/AppDownloadPage/AppDownloadPage';

function App() {
  return (
    <div>
      <NavbarTop/>
      <Router>

        <Route exact path="/" component={props => <AppDownloadPage/>} />
        <Route exact path="/newhome" component={props => <Home/>} />
        <Route exact path="/findbroker" component={props => <FindABroker/>} />

        <Route path="/insurance" exact component={props => <Homepage/>}/>
        <Route path="/compareinsurance" exact component={props => <CompareInsurance/>}/>
        <Route path="/cars" exact component={props => <YourCar/>}/>
        <Route path="/drivers" exact component={props => <Drivers/>}/>
        <Route path="/discounts" exact component={props => <Discounts/>}/>
        <Route path="/quotes" exact component={props => <Quotes/>}/>
        <Route path="/allquotes" exact component={props => <AllQuotes/>}/>

        <Route path="/how_it_works" exact component={props => <HowItWorks/>}/>
        <Route path="/explore_produces" exact component={props => <HowItWorksMob/>}/>
        <Route path="/leaderboard" exact component={props => <Leaderboard/>}/>
        <Route path="/affiliate_bank_m" exact component={props => <AffiliateBankMob/>}/>
        <Route path="/influencex_m" exact component={props => <InfluenceXMob/>}/>
        <Route path="/brokerapp_m" exact component={props => <BrokerappMob/>}/>
        <Route path="/broker_industry" exact component={props => <BrokerIndustryLand/>}/>
        <Route path="/brands" exact component={props => <Brands/>}/>
        <Route path="/brand_profile" exact component={props => <BrandProfile/>}/>

        <Route path="/register" exact component={props => <EnterCode/>}/>
        <Route path="/register_user" exact component={props => <UserMail/>}/>
        <Route path="/register_password" exact component={props => <Password/>}/>
        <Route path="/register_newpassword" exact component={props => <NewPassword/>}/>

        <Route path="/ios_registration" exact component={props => <IOSRegistration/>}/>

        <BrokerTV/>
      </Router>
      <FooterMobile/>
    </div>
  );
}

export default App;
