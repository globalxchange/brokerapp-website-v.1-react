import React, { Component } from "react";
import axios from "axios";
import NvestHealthLogo from "../Images/Nvest_health_logo.png";
import NvestHealthBlue from "../Images/Nvest_health_Blue.png";
import NvestHealthBlueLogo from "../Images/Nvest_health_blue_Logo.png";
import { message } from "antd";

const ProductContext = React.createContext();

class ProductProvider extends Component {
  state = {
    leftMenuDrw: false,
    findB: [],
    findBResult: [],
    showfindB: false,
    hoverproduces: "",
    vidOpenDrw: false,
    vidRightDrw: false,
    selectedBrokerfor: [],
    popBrandInfo: "",
    popBrandDetail: [{ id: 1, nameBrand: "Gx" }],
    brandProfDrwL: false,
    brandProfDrwR: false,
    allBrands: [],
    mAppLDrw: false,
    mAppLDrwMob: false,
    AndoridIos: [],
    appProfileInfo: [],
    brandLicen: [],
    lincenMail: "",
    mailInShow: false,
    passwordEnter: false,
    EnterPassword: "",
    onBrokerTV: "onContent",
    tvBrand: [],
    registring: "gCode",
    gxRegUsername: "",
    gxRegEmail: "",
    gxRegCreatepass: "",
    gxRegConfirmpass: "",
    gxCode: "",
    CheckLoad: false,
    gxRegEmailCode: "",
    pinOpen: false,
    vipPin: "",

    // insurance premiums ************************
    yearselect: [
      "2020",
      "2019",
      "2018",
      "2017",
      "2016",
      "2015",
      "2014",
      "2013",
      "2012",
      "2011",
      "2010",
      "2009",
      "2008",
      "2007",
      "2006",
      "2005",
      "2004",
      "2003",
      "2002",
      "2001",
      "2000",
      "1999",
      "1998",
      "1997",
      "1996",
      "1995",
      "1994",
      "1993",
      "1992",
      "1991",
    ],
    yearselected: "",
    showyear: false,
    showmakeall: false,
    makeselect: [
      { logos: NvestHealthBlue, logoname: "BMW" },
      { logos: NvestHealthLogo, logoname: "Audi" },
      { logos: NvestHealthBlueLogo, logoname: "Ford" },
      { logos: NvestHealthBlue, logoname: "Dodge" },
      { logos: NvestHealthLogo, logoname: "Mitsubishi" },
      { logos: NvestHealthBlueLogo, logoname: "Honda" },
      { logos: NvestHealthLogo, logoname: "Hyundai" },
      { logos: NvestHealthLogo, logoname: "Toyota" },
      { logos: NvestHealthBlue, logoname: "Benz" },
      { logos: NvestHealthBlue, logoname: "KIA" },
      { logos: NvestHealthBlue, logoname: "Subaru" },
      { logos: NvestHealthBlueLogo, logoname: "Land Rover" },
      { logos: NvestHealthBlueLogo, logoname: "Tesla" },
      { logos: NvestHealthBlue, logoname: "Jeep" },
    ],
    makeselected: "",
    showmake: false,
    showmodalall: false,
    modalselect: ["Series Z78", "Series A57", "F99", "A2"],
    modalselected: "",
    showmodal: false,
    showtrimall: false,
    trimselect: ["Premium", "Premium Plus", "Premium Titanium", "Coupe"],
    trimselected: "",
    showtrim: false,
    yearselectfilter: [],
    diseachway: false,
    paystat: false,
    infoyourself: false,
    discountneed: false,
    priorinsurance: false,
    hasinsuredcurrent: false,
    notinsured: false,
    whatcurcarrier: false,
    isnotcurcarrier: false,
    iscurcarrier: "",
    brokerAppUpdates: "",
  };

  menuDrawer = (side, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    this.setState({ [side]: open, vidRightDrw: false });
  };
  showInfluenceX = () => {
    this.setState({ hoverproduces: "InfluenceX" });
  };
  showBrokerapp = () => {
    this.setState({ hoverproduces: "Brokerapp" });
  };
  showAffiliateBank = () => {
    this.setState({ hoverproduces: "AffiliateBank" });
  };
  showProducesOut = () => {
    this.setState({ hoverproduces: "" });
  };
  vidDisplayDrw = (side, vidOpenDrw) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    this.setState({
      ...this.state.vidRightDrw,
      [side]: vidOpenDrw,
      leftMenuDrw: false,
    });
  };
  brokerForWhat = () => {
    axios
      .get(
        "https://storeapi.apimachine.com/dynamic/BrokerApp/whatdoyouneedabrokerfor?key=867e1ca2-8932-4eff-a3b8-e448c936821b"
      )
      .then((res) => {
        this.setState({ findB: res.data.data, findBResult: res.data.data });
        // this.setState({ findBResult: res.data.data })
      });
  };
  selectedBrokerType = async (Fbrok) => {
    this.setState({
      selectedBrokerfor: Fbrok,
    });
    await localStorage.setItem(
      "selectedBrokerfor",
      this.state.selectedBrokerfor
    );
    console.log("selectedBrokerfor", this.state.selectedBrokerfor);
  };
  hideBrands = () => {
    this.setState({ popBrandInfo: "" });
  };
  getAllBrands = () => {
    axios.get("https://teller2.apimachine.com/admin/allBankers").then((res) => {
      this.setState({ allBrands: res.data.data });
    });
  };
  showAllbrand = (Brand) => {
    this.setState({ popBrandInfo: Brand._id });
    console.log("allBrandInfo", this.state.popBrandInfo);
  };
  getAndroidIos = () => {
    axios
      .get(
        "https://storeapi.apimachine.com/dynamic/Globalxchangetoken/applinks?key=4c69ba17-af5c-4a5c-a495-9a762aba1142"
      )
      .then((res) => {
        this.setState({ AndoridIos: res.data.data });
      });
  };

  getBrokerAppUpdates = () => {
    axios
      .get(
        "https://comms.globalxchange.com/gxb/apps/mobile/app/links/logs/get",
        {
          params: { app_code: "broker_app" },
        }
      )
      .then((resp) => {
        const { data } = resp;

        if (data.status) {
          const logs = data.logs || [];

          // console.log('Logs', logs);
          // logs.sort((a, b) => a.timestamp - b.timestamp);
          this.setState({ brokerAppUpdates: logs[0] || "" });
        }
      })
      .catch((error) => {});
  };

  mailEnter = (Brand) => {
    this.setState({ mailInShow: true, appProfileInfo: Brand });
  };
  EMRemove = () => {
    this.setState({ mailInShow: false });
  };
  brokerAppProfile = async () => {
    await axios
      .get(
        `https://comms.globalxchange.com/gxb/product/get?email=${this.state.lincenMail}`
      )
      .then((res) => {
        this.setState({ brandLicen: res.data.products, mailInShow: false });
      });
  };
  // getBrandLicenses = async () => {
  //   await axios.get(`https://comms.globalxchange.com/gxb/product/get?email=${this.state.lincenMail}`)
  //   .then(res => {
  //     this.setState({ brandLicen: res.data.products })
  //     }
  //   )
  // }
  passwordOk = (e) => {
    this.setState({
      passwordEnter: true,
    });
  };
  passwordCancel = (e) => {
    this.setState({
      passwordEnter: false,
    });
  };
  moveBrokerTV = (Brand) => {
    this.setState({
      onBrokerTV: "onVideo",
      tvBrand: Brand,
    });
  };
  moveBrokerGuideC = () => {
    this.setState({
      onBrokerTV: "onContent",
    });
  };
  mailEnterProfile = (tvBrand) => {
    this.setState({ mailInShow: true, appProfileInfo: tvBrand });
  };
  gxUserMailReg = () => {
    this.setState({
      registring: "gUserMail",
    });
  };
  gxPasswordReg = () => {
    this.setState({
      registring: "gPassword",
    });
  };
  gxNewPasswordReg = () => {
    this.setState({
      registring: "gNewPassword",
    });
  };
  gXEmailCodeReg = () => {
    this.setState({
      registring: "gEmailCode",
    });
  };
  GxRegistration = (e) => {
    e.preventDefault();
    this.setState({ CheckLoad: true });
    let body = {
      username: this.state.gxRegUsername,
      email: this.state.gxRegEmail,
      password: this.state.gxRegCreatepass,
      ref_affiliate: this.state.gxCode,
      account_type: "Personal",
      signedup_app: "BrokerApp",
    };
    axios
      .post("https://gxauth.apimachine.com/gx/user/signup", body)
      .then((res) => {
        // console.log(res)
        if (res.data.status === true) {
          message.success(res.data.message, 8);
          this.setState({ CheckLoad: false, registring: "gEmailCode" });
        } else {
          this.setState({ CheckLoad: false });
          message.error(res.data.message, 8);
        }
      });
  };
  mailCodeGxRegistration = (e) => {
    e.preventDefault();
    this.setState({ CheckLoad: true });
    let body = {
      email: this.state.gxRegEmail,
      code: this.state.gxRegEmailCode,
    };
    axios
      .post("https://gxauth.apimachine.com/gx/user/confirm", body)
      .then((res) => {
        if (res.data.status === true) {
          message.success(res.data.message, 8);
          this.setState({ CheckLoad: false, registring: "gCongrats" });
        } else {
          message.error(res.data.message, 8);
          this.setState({ CheckLoad: false });
        }
        console.log("msil res", res);
      });
  };

  pinClickOpen = () => {
    this.setState({ pinOpen: true });
  };

  pinClicClose = (value) => {
    this.setState({ pinOpen: false });
  };

  // insurance premiums ************************

  inputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  yearadd = (yearsel) => {
    this.setState({
      yearselected: yearsel,
      showyear: true,
      showmakeall: true,
    });
  };
  yearRemove = () => {
    this.setState({
      yearselected: "",
      showyear: false,
      showmakeall: false,
      showmake: false,
      showmodalall: false,
      showmodal: false,
      showtrim: false,
      showtrimall: false,
    });
  };
  makeAdd = (NHbrand) => {
    this.setState({
      makeselected: NHbrand.logoname,
      showmake: true,
      showmakeall: false,
      showmodalall: true,
    });
  };
  makeRemove = () => {
    this.setState({
      showmake: false,
      showyear: true,
      showmakeall: true,
      showmodalall: false,
      showmodal: false,
      showtrim: false,
      showtrimall: false,
    });
  };
  modalAdd = (modalNH) => {
    this.setState({
      modalselected: modalNH,
      showmodal: true,
      showmodalall: false,
      showtrimall: true,
    });
  };
  modalRemove = () => {
    this.setState({
      showmake: true,
      showmodal: false,
      showmodalall: true,
      showtrim: false,
      showtrimall: false,
    });
  };
  trimAdd = (trimNH) => {
    this.setState({
      trimselected: trimNH,
      showtrim: true,
    });
  };
  trimRemove = () => {
    this.setState({
      showtrim: false,
    });
  };

  filterList = async (event) => {
    event.preventDefault();
    let inval = event.target.value;
    let updatedList = this.state.yearselect;
    // updatedList.sort() ;
    updatedList = updatedList.filter((item) => {
      return (
        item.toLowerCase().search(inval.toLowerCase()) !== -1 ||
        item.toLowerCase().search(inval.toLowerCase()) !== -1
      );
    });
    await this.setState({ yearselectfilter: updatedList });
    // console.log("updated",updatedList)
  };

  eachWayDist = () => {
    this.setState({
      diseachway: true,
    });
  };
  payStat = () => {
    this.setState({
      paystat: true,
    });
  };
  infoYourself = () => {
    this.setState({
      infoyourself: true,
    });
  };
  priorInsurance = () => {
    this.setState({
      discountneed: true,
      priorinsurance: true,
    });
  };
  haveInsurance = () => {
    this.setState({
      hasinsuredcurrent: true,
      notinsured: false,
    });
  };
  notHaveInsurance = () => {
    this.setState({
      hasinsuredcurrent: false,
      notinsured: true,
      whatcurcarrier: false,
      isnotcurcarrier: false,
    });
  };
  yourCurrentCarrier = () => {
    this.setState({
      whatcurcarrier: true,
    });
  };
  yourCurrentCarrierSelected = (carrierBrand) => {
    this.setState({
      iscurcarrier: carrierBrand.clogoname,
      whatcurcarrier: false,
      hasinsuredcurrent: false,
      isnotcurcarrier: true,
    });
  };
  removeCurrentCarrier = () => {
    this.setState({
      whatcurcarrier: true,
      hasinsuredcurrent: true,
      isnotcurcarrier: false,
    });
  };
  brokerFinding = async (e) => {
    e.preventDefault();
    let typeval = e.target.value;
    if (typeval.length > 0) {
      this.setState({ showfindB: true });
    } else if (typeval.length === 0) {
      this.setState({ showfindB: false });
    }
    let findbrokerList = this.state.findB;
    findbrokerList = findbrokerList.filter((brokerF) => {
      return (
        brokerF.formData.Name.toLowerCase().search(typeval.toLowerCase()) !== -1
      );
    });
    await this.setState({ findBResult: findbrokerList });
    console.log("findBResult", this.state.showfindB);
  };
  componentWillMount() {
    this.setState({ findBResult: this.state.findB });
  }

  componentDidMount() {
    this.setState({
      yearselectfilter: this.state.yearselect,
      selectedBrokerfor: localStorage.getItem("selectedBrokerfor"),
    });
    this.brokerForWhat();
    this.getAllBrands();
    this.getAndroidIos();
    // this.getBrandLicenses();
    this.getBrokerAppUpdates();
  }

  render() {
    return (
      <ProductContext.Provider
        value={{
          ...this.state,

          menuDrawer: this.menuDrawer,
          brokerFinding: this.brokerFinding,
          showInfluenceX: this.showInfluenceX,
          showBrokerapp: this.showBrokerapp,
          showAffiliateBank: this.showAffiliateBank,
          showProducesOut: this.showProducesOut,
          vidDisplayDrw: this.vidDisplayDrw,
          selectedBrokerType: this.selectedBrokerType,
          hideBrands: this.hideBrands,
          // showGxBrand: this.showGxBrand,
          // showTfBrand: this.showTfBrand,
          // showBosBrand: this.showBosBrand,
          // showOxygenBrand: this.showOxygenBrand,
          // showMAllsBrand: this.showMAllsBrand,
          // showSnapayBrand: this.showSnapayBrand,
          showAllbrand: this.showAllbrand,
          brokerAppProfile: this.brokerAppProfile,
          // getBrandLicenses: this.getBrandLicenses,
          mailEnter: this.mailEnter,
          EMRemove: this.EMRemove,
          passwordOk: this.passwordOk,
          passwordCancel: this.passwordCancel,
          moveBrokerTV: this.moveBrokerTV,
          moveBrokerGuideC: this.moveBrokerGuideC,
          mailEnterProfile: this.mailEnterProfile,
          GxRegistration: this.GxRegistration,
          gxUserMailReg: this.gxUserMailReg,
          gxPasswordReg: this.gxPasswordReg,
          gxNewPasswordReg: this.gxNewPasswordReg,
          mailCodeGxRegistration: this.mailCodeGxRegistration,
          pinClickOpen: this.pinClickOpen,
          pinClicClose: this.pinClicClose,

          // insurance premiums ************************
          inputChange: this.inputChange,
          yearadd: this.yearadd,
          yearRemove: this.yearRemove,
          makeAdd: this.makeAdd,
          makeRemove: this.makeRemove,
          modalAdd: this.modalAdd,
          modalRemove: this.modalRemove,
          trimAdd: this.trimAdd,
          trimRemove: this.trimRemove,
          filterList: this.filterList,
          eachWayDist: this.eachWayDist,
          payStat: this.payStat,
          infoYourself: this.infoYourself,
          priorInsurance: this.priorInsurance,
          haveInsurance: this.haveInsurance,
          notHaveInsurance: this.notHaveInsurance,
          yourCurrentCarrier: this.yourCurrentCarrier,
          yourCurrentCarrierSelected: this.yourCurrentCarrierSelected,
          removeCurrentCarrier: this.removeCurrentCarrier,
        }}
      >
        {this.props.children}
      </ProductContext.Provider>
    );
  }
}

const ProductConsumer = ProductContext.Consumer;

export { ProductProvider, ProductConsumer };
